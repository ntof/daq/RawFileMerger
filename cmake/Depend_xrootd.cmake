
include(Tools)

find_library(XROOTD_LIBRARIES XrdCl)
find_path(XROOTD_INCLUDE_DIRS XrdCl/XrdClFileSystem.hh PATH_SUFFIXES xrootd)
find_path(XROOTD_PRIVATE_INCLUDE_DIRS XrdCl/XrdClPostMaster.hh PATH_SUFFIXES xrootd/private)

assert(XROOTD_LIBRARIES AND XROOTD_INCLUDE_DIRS
    MESSAGE "Failed to find xrootd package")

if(XROOTD_PRIVATE_INCLUDE_DIRS)
  set(HAVE_XROOTD_PRIVATE TRUE CACHE INTERNAL "xrootd private headers")
  list(APPEND XROOTD_INCLUDE_DIRS "${XROOTD_PRIVATE_INCLUDE_DIRS}")
else()
  # disables optional failsafe features
  message(WARNING "XRootd private headers not found")
endif()
