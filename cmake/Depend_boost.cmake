include(Tools)

find_package(Boost REQUIRED system filesystem program_options thread)

find_library(PTHREAD_LIBRARIES pthread)
if(PTHREAD_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${PTHREAD_LIBRARIES})
endif()
