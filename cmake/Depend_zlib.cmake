
include(Tools)

find_library(ZLIB_LIBRARIES z)
find_path(ZLIB_INCLUDE_DIRS zlib.h)

assert(ZLIB_LIBRARIES AND ZLIB_INCLUDE_DIRS
  MESSAGE "Failed to find zlib package")
