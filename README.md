# RawFileMerger

Acquisition files archiver for n_ToF experiment.

## Build

To build this project it is recommended to install [x-builder](https://gitlab.cern.ch/ntof/daq/common/tools/x-builder) script.

```bash
docker-builder

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Testing

To run unit-tests:
```bash
docker-builder ./build/tests/test_all
```

To run linters:
```bash
docker-builder make -C build lint
```

## Running

To run this service on a local DIM/DNS node, edit `tests/data/configMisc.xml` to set the DIM/DNS address.

Then to connect the service from a container to this DNS:
```bash
docker-builder
export DIM_HOST_NODE=$(hostname -i)
./build/src/RawFileMerger -c ./tests/data/conf.xml -m ./tests/data/configMisc.xml

# Alternatively, one can also expose service directly on current host:
OPTS='--network=host --userns=host' docker-builder
./build/src/RawFileMerger -c ./tests/data/conf.xml -m ./tests/data/configMisc.xml
```

## Debugging

To compile this component for gdb/lldb:
```bash
docker-builder

cd build && cmake3 -DCMAKE_BUILD_TYPE=Debug ..
```
