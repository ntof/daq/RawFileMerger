/*
 * Main.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: mdonze
 */
#include <iostream>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <NTOFLogging.hpp>

#include "Config.h"
#include "NTOFException.h"
#include "RFMManager.hpp"

#include <dic.hxx>
#include <dis.hxx>

using namespace ntof::rfm;
namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char **argv)
{
    ntof::log::init(argc, argv);
    try
    {
        std::string configFile;
        std::string configMisc;
        std::string logConfFile;
        int vLevel;
        // Parse program option
        po::options_description desc("Program usage");
        desc.add_options()("help", "produce this help message")(
            "verbose", "verbose mode")("config,c",
                                       po::value<std::string>(&configFile)
                                           ->default_value(Config::configFile),
                                       "force the configuration file")(
            "misc-config,m",
            po::value<std::string>(&configMisc)
                ->default_value(Config::configMiscFile),
            "force the misc configuration file")("noremove,n",
                                                 "don't delete local files")(
            "logconf,l",
            po::value<std::string>(&logConfFile)
                ->default_value("/etc/ntof/rfmerger_logger.conf"),
            "force logger configuration file")(
            "v,v", po::value<int>(&vLevel)->default_value(0),
            "Sets verbose level");
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 0;
        }

        po::notify(vm);

        // Initialize logger
        el::Configurations confFromFile(logConfFile);
        el::Loggers::reconfigureAllLoggers(confFromFile);

        Config::load(configFile, configMisc);

        Config &conf = Config::instance();
        if (vm.count("noremove") != 0)
            conf.setDeleterEnabled(true);

        DimServer::setDnsNode(conf.getDimDns().c_str(), conf.getDimDnsPort());
        DimClient::setDnsNode(conf.getDimDns().c_str(), conf.getDimDnsPort());

        bfs::create_directories(
            bfs::path(conf.getPaths().database).parent_path());

        RFMManager man;
        man.initialize();

        DimServer::start(conf.getDimPrefix().c_str());

        man.run();
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR) << "Caught n_TOF exception : " << ex.what();
        return -1;
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "Caught exception : " << ex.what();
        return -1;
    }
    catch (...)
    {
        LOG(ERROR) << "Caught unknown exception...";
        return -1;
    }
    return 0;
}
