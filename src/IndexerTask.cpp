/*
 * IndexerTask.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: mdonze
 */
#include "IndexerTask.h"

#include <cstdint>

#include <DaqTypes.h>
#include <NTOFLogging.hpp>

#include "Config.h"
#include "Paths.hpp"
#include "RFMException.hpp"
#include "RFMFactory.hpp"
#include "RawHeaders.h"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

IndexerTask::EventDetails::EventDetails(const FileEvent &event) :
    streamNumber(1),
    fileNumber(event.fileNumber()),
    sequenceNumber(event.sequenceNumber()),
    trigNumber(event.eventNumber()),
    fileOffset(event.offset())
{}

IndexerTask::IndexerTask(MergedFileIndex &index) :
    m_run(RFMFactory::instance().getRun(index.runNumber())),
    m_index(index.shared()),
    m_offset(0)
{
    VLOG(1) << "[IndexerTask]: Constructing IndexerTask object with run #"
            << m_index->runNumber();
}

IndexerTask::~IndexerTask()
{
    VLOG(1) << "[IndexerTask]: Destroying IndexerTask object with run #"
            << m_index->runNumber();
}

void IndexerTask::run()
{
    std::ostringstream errMsg;
    Archiver &archiver = Archiver::instance();
    try
    {
        m_lockFile = RFMLockFile::instance().getLockFile();

        if (!m_run || !m_index)
            throw NTOFException("can't merge null index file", __FILE__,
                                __LINE__);
        else if (!m_run->hasFiles())
        {
            LOG(INFO) << "[IndexerTask]: ignoring empty index run #"
                      << m_index->runNumber();
            m_index->setSize(0);
            m_index->setStatus(MergedFile::IGNORED);
            m_index->save();
            taskSignal(std::string());
            return;
        }

        LOG(TRACE) << "Building index file for run #" << m_index->runNumber();

        bfs::path resFile = Paths::getRemoteFilePath(*m_run, *m_index);
        // Create the CASTOR folder (and parent sub folders)
        archiver.createDir(resFile.parent_path());

        // Query CASTOR file information
        Archiver::FileInfo info = archiver.getInfo(resFile);
        if (info)
        {
            Config::ReplaceStrategy replaceStrategy =
                Config::instance().getReplaceStrategy();
            // File exists on CASTOR
            if (replaceStrategy == Config::ReplaceStrategy::DELETE)
            {
                LOG(INFO) << "[IndexerTask]: File " << resFile
                          << " already exists on archiver, deleting it...";
                archiver.deleteFile(resFile);
            }
            else if (replaceStrategy == Config::ReplaceStrategy::RENAME)
            {
                LOG(INFO) << "[IndexerTask]: File " << resFile.string()
                          << " already exists on archiver, renaming it...";

                bfs::path oldFile = resFile;
                oldFile += ".old";
                // Remove previous existing .old file
                if (archiver.getInfo(oldFile))
                    archiver.deleteFile(oldFile);

                archiver.renameFile(resFile, oldFile);
            }
            else if (replaceStrategy == Config::ReplaceStrategy::IGNORE)
            {
                LOG(INFO) << "[IndexerTask]: File " << resFile
                          << " already exists on archiver, ignoring it...";

                return;
            }
        }
        // Update file status (merging will start now)
        m_index->setStatus(MergedFile::TRANSFERRING);
        m_index->save();

        // Starts to write index file
        // Opens remote file
        Archiver::File::Shared file = archiver.open(resFile);
        m_offset = 0; // We start at the beginning of the file

        // First, add RCTR and MODH
        appendRunHeaders(*file);

        std::vector<EventDetails> details;
        MergedFile::EventsList events;
        for (MergedFile::Shared &mergedFile : m_run->getFiles())
        {
            MergedFile::EventsList fileEvents = mergedFile->getEvents();
            events.insert(events.end(), fileEvents.begin(), fileEvents.end());
            for (FileEvent::Shared &event : fileEvents)
            {
                details.push_back(std::move(EventDetails(*event)));
            }
        }

        LOG(TRACE) << "Run #" << m_index->runNumber() << " contains "
                   << events.size() << " events";
        appendIndexHeader(*file, details);
        details.clear(); // freeing up some memory

        // Adds events headers and ADDH
        appendEventHeaders(*file, events);

        file->close();
        file.reset();

        // Merging finished, update the database
        m_index->setStatus(MergedFile::COPIED);
        m_index->setSize(m_offset);
        m_index->save();

        std::ostringstream oss;
        oss << "/run" << m_index->runNumber() << ".done";
        bfs::path fileDonePath = resFile.parent_path();

        fileDonePath += oss.str();
        Archiver::File::Shared fileDone = archiver.open(fileDonePath);
        fileDone->close();
        fileDone.reset();
        // Task completed
        taskSignal(std::string());
        return;
    }
    catch (const RFMException &ex)
    {
        errMsg << "RFMException while indexing run #" << m_index->runNumber()
               << " : " << ex.what();
    }
    catch (const NTOFException &ex)
    {
        errMsg << "NTOFException while indexing run #"
               << std::to_string(m_index ? m_index->runNumber() : -1) << " : "
               << ex.getMessage();
    }
    catch (const std::exception &ex)
    {
        errMsg << "std::exception while indexing run #" << m_index->runNumber()
               << " : " << ex.what();
    }
    catch (...)
    {
        errMsg << "unknown exception while indexing run #"
               << m_index->runNumber();
    }

    std::string err = errMsg.str();
    err = err.empty() ? std::string("unknown error") : err;
    LOG(ERROR) << "[IndexerTask] " << err;
    taskSignal(err);
}

void IndexerTask::appendRunHeaders(Archiver::File &out)
{
    LOG(TRACE) << "Appending RCTR and MODH to index file, run #"
               << m_index->runNumber();

    const std::string filePath = Paths::getRunFilePath(*m_run).string();

    LOG(TRACE) << "Opening local file " << filePath;
    std::ifstream file(filePath.c_str(),
                       std::ios::in | std::ios::binary | std::ios::ate);
    if (!file.is_open())
    {
        throw RFMException(std::string("Unable to open file: ") + filePath);
    }

    std::vector<char> buffer(file.tellg());
    file.seekg(0, std::ios::beg);

    vstreambuf vs(buffer);
    std::ostream os(&vs);

    while (file.good())
    {
        HeaderLookup hl;
        file >> hl;

        if (file.eof())
        {
            // must read past end to trigger eof (and there may be some padding)
            file.clear();
            break;
        }

        switch (hl.type)
        {
        case RunControlHeader::TITLE: {
            RunControlHeader rctr;
            file >> rctr;
            rctr.segmentNumber = 0;
            rctr.streamNumber = 0;
            os << rctr;
            break;
        }
        case ModuleHeader::TITLE: {
            ModuleHeader modh;
            file >> modh;
            os << modh;
            break;
        }
        default:
            throw RFMException(std::string("unexpected header in run file: ") +
                               filePath);
        }
    }
    if (file.fail() || os.fail())
    {
        throw RFMException(
            std::string("failed to prepare RCTR and MODH for file: ") +
            filePath);
    }
    file.close();
    writeData(out, buffer.size(), buffer.data());
}

void IndexerTask::appendIndexHeader(Archiver::File &file,
                                    const std::vector<EventDetails> &events)
{
    HEADER index;
    index.title[0] = 'I';
    index.title[1] = 'N';
    index.title[2] = 'D';
    index.title[3] = 'X';
    index.reserved = 0;
    index.revNumber = 1;
    index.nbWords = (sizeof(EventDetails) / sizeof(int32_t)) * events.size();
    writeData(file, sizeof(index), &index);
    // Now, write all data in vector (should be contigous...)
    writeData(file, sizeof(EventDetails) * events.size(), &events[0]);
}

void IndexerTask::appendEventHeaders(Archiver::File &out,
                                     const MergedFile::EventsList &events)
{
    for (const FileEvent::Shared &event : events)
    {
        LOG(TRACE) << "Appending EVEH and ADDH to index file, run #"
                   << m_index->runNumber() << " timing event #"
                   << event->eventNumber();

        std::string filePath = Paths::getEventFilePath(*m_run, *event).string();
        LOG(TRACE) << "Opening local file " << filePath;
        std::ifstream file(filePath.c_str(),
                           std::ios::in | std::ios::binary | std::ios::ate);
        if (!file.is_open())
        {
            throw RFMException(std::string("Unable to open file: ") + filePath);
        }

        std::streampos size = file.tellg();
        file.seekg(0, std::ios::beg);

        // Compute size of event, for index files, it's this file lenght
        uint32_t evtSize = size;
        // Convert size of event to words
        evtSize /= sizeof(int32_t);

        EventHeader eveh;
        file >> eveh;
        if (!file.good())
        {
            throw RFMException(std::string("EVEH missing in ") + filePath);
        }
        eveh.eventNumber = event->sequenceNumber();
        eveh.sizeOfEvent = evtSize;
        {
            std::vector<char> buffer(size);
            vstreambuf vs(buffer);
            std::ostream os(&vs);
            os << eveh;
            file.read(&buffer.data()[EventHeader::SIZE],
                      size_t(size) - EventHeader::SIZE);
            if (!file.good())
            {
                throw RFMException(std::string("Unable to load file: ") +
                                   filePath);
            }
            // Write data to CASTOR file
            writeData(out, size, buffer.data());
        }
    }
}

/**
 * Write data to CASTOR file and update fOffset
 * @param cFile
 * @param size
 * @param buffer
 */
void IndexerTask::writeData(Archiver::File &archive,
                            uint32_t size,
                            const void *buffer)
{
    archive.write(m_offset, size, buffer);
    m_offset += size;
    m_index->setTransferred(m_offset);
}
