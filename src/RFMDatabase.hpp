/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-26T09:12:28+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMDATABASE_HPP__
#define RFMDATABASE_HPP__

#include <ctime>
#include <map>
#include <string>
#include <vector>

#include <sqlite3.h>

#include "data/DaqInfo.hpp"
#include "data/FileEvent.hpp"
#include "data/FileStats.hpp"
#include "data/MergedFile.hpp"
#include "data/MergedFileIndex.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class RFMDatabase
{
public:
    typedef std::set<RunInfo::RunNumber> RunNumberList;

    explicit RFMDatabase(const std::string &dbName);
    ~RFMDatabase();

    inline const std::string &errorString() const { return m_errorString; }

    /**
     * @brief insert a RunInfo into the database
     * @return the inserted object, an empty shared_ptr if it exists or
     * couldn't be inserted
     */
    RunInfo::Shared insertRun(RunInfo::RunNumber runNumber,
                              const std::string &expArea,
                              std::time_t startDate,
                              const std::set<DaqInfo::CrateId> &daqs,
                              const std::string &experiment = std::string());

    /**
     * @brief get information about a run
     * @param[in]  runNumber the run to retrieve
     */
    RunInfo::Shared getRun(RunInfo::RunNumber runNumber);

    /**
     * @brief entirely remove this run and associated files/events ...
     * @param[in] runNumber run to remove
     */
    bool removeRun(RunInfo::RunNumber runNumber);

    /**
     * @brief retrieve all runs that are fully migrated
     * @param[out] runs list of uncompleted runs
     * @return true on success
     */
    bool getMigratedRuns(RunNumberList &runs);

    /**
     * @brief get all runs from db
     */
    bool getAllRuns(RunNumberList &list);

    /**
     * @brief update run information in database
     * @param[in]  run the run to store
     * @return true if information could be updated, false otherwise
     */
    bool update(RunInfo &run);

    /**
     * @brief insert a MergedFile into the database
     * @return the inserted object, an empty shared_ptr if it exists or
     * couldn't be inserted
     */
    MergedFile::Shared insertMergedFile(const RunInfo &run,
                                        MergedFile::FileNumber sequence);

    /**
     * @brief get merged files list
     * @param[in]  runNumber the associated run number
     * @param[out] out output MergedFile list
     */
    bool getMergedFiles(const RunInfo &run,
                        std::vector<MergedFile::Shared> &out,
                        MergedFileIndex::Shared &index);

    /**
     * @brief update file information in database
     * @param  file the file to store
     * @return true if information could be updated, false otherwise
     */
    bool update(MergedFile &file);

    /**
     * @brief insert a new file event
     * @param[in]  file associated merged file
     * @param[in]  eventNumber event number
     * @param[in]  event estimated size
     * @param[in]  offset event offset in merged file
     */
    FileEvent::Shared insertFileEvent(const MergedFile &file,
                                      FileEvent::EventNumber eventNumber,
                                      FileEvent::SequenceNumber seqNum,
                                      std::size_t size,
                                      std::size_t offset = std::size_t(-1));

    /**
     * @brief get file events list
     * @param[in]  file the associated file
     * @param[out]  out the output FileEvent list
     * @param[out]  totalSize total events estimated size
     */
    bool getFileEvents(const MergedFile &file,
                       std::vector<FileEvent::Shared> &out,
                       std::size_t &totalSize);

    /**
     * @brief update event information in database
     * @param[in]  event the event to store
     * @return true if information could be updated, false otherwise
     */
    bool update(FileEvent &event);

    /**
     * @brief get general file statistics
     */
    bool getFileStats(FileStats &stats);

    /**
     * @brief retrieve next retry timestamp
     * @param[out]  runNumber next retry runNumber
     * @param[out]  fileNumber next retry fileNumber
     * @param[out]  date       next retry date
     * @return true on success
     * @details may output RunInfo::InvalidRunNumber if nothing is available
     */
    bool getNextRetry(RunInfo::RunNumber &runNumber,
                      MergedFile::FileNumber &fileNumber,
                      std::time_t &date);

    /**
     * @brief get next expiry timestamp
     * @param[out]  runNumber next expiring runNumber
     * @param[out]  date       next expiring date
     * @return true on success
     */
    bool getNextExpiry(RunInfo::RunNumber &runNumber, std::time_t &date);

protected:
    enum Stmt
    {
        RunInsert,
        RunGet,
        RunUpdate,
        RunDelete,
        FileInsert,
        FileGet,
        FileUpdate,
        FileRetriesUpdate,
        FileRetriesRemove,
        FileGetStats,
        EventInsert,
        EventGet,
        EventUpdate,
        FileGetNextRetry,
        RunGetNextExpiry,
        RunGetMigrated
    };
    typedef std::map<Stmt, sqlite3_stmt *> StmtMap;

    template<class C>
    std::shared_ptr<C> makeShared(C *c);

    void createDb();
    bool check(int rc, const char *msg, char *sqliteMsg = nullptr);
    bool updateRetries(RunInfo::RunNumber runNumber,
                       MergedFile::FileNumber fileNumber,
                       uint32_t retries,
                       std::time_t retryDate);
    bool removeRetries(RunInfo::RunNumber runNumber,
                       MergedFile::FileNumber fileNumber);

    std::string m_errorString;
    sqlite3 *m_db;
    StmtMap m_stmts;
};

} // namespace rfm
} // namespace ntof

#endif
