/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-25T18:10:04+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMMONITORINGHANDLERS_HPP__
#define RFMMONITORINGHANDLERS_HPP__

#include <string>
#include <vector>

#include <DIMParamList.h>
#include <DIMXMLRpc.h>

namespace ntof {
namespace rfm {

class RFMMonitoring;

class RunsParamListHandler : public ntof::dim::DIMParamListHandler
{
public:
    virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const ntof::dim::DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) override;
};

class CurrentRunParamListHandler : public ntof::dim::DIMParamListHandler
{
public:
    explicit CurrentRunParamListHandler(RFMMonitoring &mon);

    virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const ntof::dim::DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) override;

protected:
    RFMMonitoring &m_mon;
};

class FilesRpc : public ntof::dim::DIMXMLRpc
{
public:
    explicit FilesRpc(const std::string &service);

    virtual void rpcReceived(ntof::dim::DIMCmd &cmd) override;
};

} // namespace rfm
} // namespace ntof

#endif
