/*
 * MergerTask.h
 *
 *  Created on: Oct 20, 2015
 *      Author: mdonze
 */

#ifndef MERGERTASK_H_
#define MERGERTASK_H_

#include <fstream>
#include <memory>

#include "Archiver.hpp"
#include "RFMLockFile.hpp"
#include "RFMManager.hpp"
#include "RFMUtils.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class RawFile
{
public:
    typedef std::shared_ptr<RawFile> Shared;
    /**
     * Constructor
     * It will open the file descriptor
     * @param fileName Name of the file on the file system
     */
    explicit RawFile(const std::string &fileName);
    virtual ~RawFile();

    /**
     * Gets the event size stored into this raw file
     * @return
     */
    uint64_t getEventSize();

    /**
     * Close file if open
     */
    void closeFile();

    /**
     * Gets the event start offset
     * @return
     */
    inline uint64_t getEventStart() { return start_; }

    /**
     * Gets the event end offset
     * @return
     */
    inline uint64_t getEventEnd() { return end_; }

    /**
     * Gets pointer to file
     * @return
     */
    inline std::ifstream &getFile() { return file_; }

private:
    RawFile(const RawFile &other);
    RawFile &operator=(const RawFile &other); // disable copy

    uint64_t start_;
    uint64_t end_;
    std::ifstream file_;
};

class MergerTask : public RFMManager::Task
{
public:
    typedef std::vector<RawFile::Shared> RawFilesList;

    explicit MergerTask(MergedFile &file);
    virtual ~MergerTask();

    /**
     * Runs the merging task
     */
    void run() override;

    /**
     * Gets CASTOR file associated with this task
     * @return
     */
    inline MergedFile::Shared mergedFile() const { return m_file; };

    /**
     * Sets the buffer size
     * @param size
     */
    static void setBufferSize(uint64_t size);

    TaskSignal taskSignal;

protected:
    void mergeToCASTOR(const boost::filesystem::path &castorPath);

    /**
     * @brief Appends RCTR and MODH to the archiver file
     * @param[in,out] file file to write
     * @param[out] list of daqs extracted from MODH
     */
    void appendRunHeaders(Archiver::File &file, RunInfo::DaqsList &daqs);

    /**
     * Appends the EVEH and ADDH header to CASTOR file
     */
    void appendEventHeaders(Archiver::File &file,
                            FileEvent &evt,
                            const RawFilesList &rawFiles);

    /**
     * Append RAW data to the CASTOR file
     */
    void appendRawData(Archiver::File &file, RawFile &rawFile);

    /**
     * Write data to CASTOR file and update fOffset
     */
    void writeData(Archiver::File &file, uint32_t size, const void *buffer);

    /**
     * Updates CASTOR file statistics
     */
    void updateStatistics();

    /**
     * Renames an existing file on CASTOR
     * @param resFile
     */
    void renameExistingCASTORFile(const boost::filesystem::path &resFile);

    RunInfo::Shared m_run; //!< needed to keep this run in cache
    MergedFile::Shared m_file;
    uint64_t m_offset; //!< CASTOR file actual position

    static uint64_t bufferSize; //!< Buffer size to read/write raw data
    RFMLockFile::LockFile m_lockFile;
};

} // namespace rfm
} /* namespace ntof */

#endif /* MERGERTASK_H_ */
