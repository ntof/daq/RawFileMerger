/*
 * FileDeleter.hpp
 *
 *  Created on: Nov 5, 2015
 *      Author: mdonze
 */

#ifndef FILEDELETER_H_
#define FILEDELETER_H_

#include <condition_variable>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <vector>

#include <Thread.hpp>

#include "RFMUtils.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class FileDeleter : public ntof::utils::Thread
{
public:
    FileDeleter();
    ~FileDeleter() override = default;

    /**
     * @brief add a new file to be monitored
     */
    void add(const MergedFile &file);

    /**
     * @brief add a new run to be monitored
     */
    void add(const RunInfo &run);

    /**
     * @brief set the deleter loop interval
     * @param interval loop delay in miliseconds
     */
    void setInterval(uint32_t interval);

    ErrorSignal errorSignal;
    FileErrorSignal fileErrorSignal;

protected:
    typedef std::map<std::string, MergedFile::Shared> MergedFileMap;
    typedef std::map<RunInfo::RunNumber, RunInfo::Shared> RunMap;

    void thread_enter() override;
    void thread_func() override;

    std::string uid(const MergedFile &file) const;

    void checkFiles(MergedFileMap &files, RunMap &runs);
    void checkRuns(RunMap &runs);
    void removeOldRuns();

    void removeRawFiles(const RunInfo &run, const MergedFile &file);

    void removeRunFiles(const RunInfo &run);

    std::mutex m_lock;
    bool m_prevError;

    MergedFileMap m_files;
    RunMap m_runs;
};

} // namespace rfm
} /* namespace ntof */

#endif /* FILEDELETER_H_ */
