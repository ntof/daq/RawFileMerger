/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-04T15:44:35+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "LocalArchiver.hpp"

#include <cerrno>
#include <sstream>

#include <NTOFLogging.hpp>

#include "Config.h"

namespace bfs = boost::filesystem;
namespace errc = boost::system::errc;
using namespace ntof::rfm;

const std::string LocalArchiver::NAME("LocalArchiver");

LocalFile::LocalFile(LocalArchiver &archiver, const std::string &filename) :
    m_archiver(archiver), m_filename(filename), m_pos(0)
{
    m_file.open(filename,
                std::ios_base::out | std::ios_base::trunc |
                    std::ios_base::binary);
}

LocalFile::~LocalFile()
{
    if (m_file.is_open())
        m_file.close();
}

void LocalFile::write(uint64_t offset, uint32_t size, const void *buffer)
{
    if (size == 0)
        return;

    if (std::streampos(offset) != m_pos)
    {
        m_file.seekp(offset);
        m_pos = offset;
    }
    m_file.write(static_cast<const char *>(buffer), size);
    if (m_file.fail())
    {
        std::ostringstream oss;
        oss << "unable to write Local file: \"" << ::strerror(errno) << "\"";
        LOG(ERROR) << "[LocalArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
    m_pos += size;
}

void LocalFile::close()
{
    m_file.close();
}

const std::string &LocalArchiver::name() const
{
    return NAME;
}

LocalArchiver::File::Shared LocalArchiver::open(const bfs::path &file)
{
    return LocalArchiver::File::Shared(new LocalFile(*this, file.string()));
}

bool LocalArchiver::isMigrated(const bfs::path &file)
{
    return bfs::exists(file);
}

LocalArchiver::FileInfo LocalArchiver::getInfo(const bfs::path &file)
{
    if (bfs::is_directory(file))
        return LocalArchiver::FileInfo(true, 0);
    else if (bfs::exists(file))
        return LocalArchiver::FileInfo(true, bfs::file_size(file));
    else
        return LocalArchiver::FileInfo();
}

void LocalArchiver::createDir(const bfs::path &file)
{
    try
    {
        bfs::create_directories(file);
    }
    catch (bfs::filesystem_error &ex)
    {
        std::ostringstream oss;
        oss << "cannot create directory " << file.string() << ": " << ex.what();
        LOG(ERROR) << "[LocalArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}

void LocalArchiver::deleteFile(const bfs::path &file)
{
    try
    {
        if (bfs::is_directory(file))
            throw bfs::filesystem_error(
                "cannot delete directories",
                errc::make_error_code(errc::invalid_argument));
        bfs::remove(file);
    }
    catch (bfs::filesystem_error &ex)
    {
        std::ostringstream oss;
        oss << "failed to delete file " << file.string() << ": " << ex.what();
        LOG(ERROR) << "[LocalArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}

void LocalArchiver::renameFile(const bfs::path &file, const bfs::path &newFile)
{
    try
    {
        bfs::rename(file, newFile);
    }
    catch (bfs::filesystem_error &ex)
    {
        std::ostringstream oss;
        oss << "failed to rename file " << file.string() << ": " << ex.what();
        LOG(ERROR) << "[LocalArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}
