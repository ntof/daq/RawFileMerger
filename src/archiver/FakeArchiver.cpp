/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T13:21:07+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "FakeArchiver.hpp"

#include <easylogging++.h>

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

const std::string FakeArchiver::NAME("FakeArchiver");

const std::string &FakeArchiver::name() const
{
    return FakeArchiver::NAME;
}

class FakeFile : public Archiver::File
{
public:
    FakeFile(FakeArchiver &archiver, const bfs::path &filename) :
        m_archiver(archiver), m_filename(filename)
    {
        LOG(INFO) << "[FakeFile]::constructor " << filename.string();
    }

    ~FakeFile()
    {
        LOG(INFO) << "[FakeFile]::destructor " << m_filename.string();
    }

    void write(uint64_t offset, uint32_t size, const void * /*buffer*/) override
    {
        LOG(INFO) << "[FakeFile]::write at:" << offset << " size:" << size
                  << " file:" << m_filename.string();
        m_archiver.addFakeFile(m_filename, size + offset);
    }

    void close() override {}

    FakeArchiver &m_archiver;
    const bfs::path m_filename;
};

Archiver::File::Shared FakeArchiver::open(const bfs::path &file)
{
    addFakeFile(file, 0);
    return Archiver::File::Shared(new FakeFile(*this, file));
}

bool FakeArchiver::isMigrated(const bfs::path &file)
{
    LOG(INFO) << "[FakeArchiver]::isMigrated on " << file.string();
    std::lock_guard<std::mutex> lock(m_lock);
    FakeFiles::iterator it = m_vfs.find(file);
    return !(it == m_vfs.end());
}

Archiver::FileInfo FakeArchiver::getInfo(const bfs::path &file)
{
    LOG(INFO) << "[FakeArchiver]::getInfo on " << file.string();
    std::lock_guard<std::mutex> lock(m_lock);
    FakeFiles::iterator it = m_vfs.find(file);
    if (it == m_vfs.end())
        return Archiver::FileInfo();
    else
        return Archiver::FileInfo(true, it->second);
}

void FakeArchiver::createDir(const bfs::path &file)
{
    LOG(INFO) << "[FakeArchiver]::createDir on " << file.string();
}

void FakeArchiver::deleteFile(const bfs::path &file)
{
    LOG(INFO) << "[FakeArchiver]::deleteFile on " << file.string();
    std::lock_guard<std::mutex> lock(m_lock);
    m_vfs.erase(file);
}

void FakeArchiver::renameFile(const bfs::path &file, const bfs::path &newFile)
{
    LOG(INFO) << "[FakeArchiver]::renameFile " << file.string() << " -> "
              << newFile.string();
    std::lock_guard<std::mutex> lock(m_lock);
    FakeFiles::iterator it = m_vfs.find(file);
    if (it == m_vfs.end())
    {
        LOG(ERROR)
            << "[FakeArchiver]: can't rename unknown file: " << file.string();
        return;
    }
    m_vfs[newFile] = it->second;
    m_vfs.erase(it);
}

void FakeArchiver::addFakeFile(const bfs::path &file, uint64_t size)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_vfs[file] = size;
}

FakeArchiver::FakeFiles FakeArchiver::files() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_vfs;
}
