/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T13:32:18+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "XRootArchiver.hpp"

#include <cstdint>
#include <mutex>
#include <sstream>

#include <NTOFLogging.hpp>
#include <zlib.h>

#include "Config.h"
#include "RFMUtils.hpp"
#include "local-config.h"

#include <XrdCl/XrdClDefaultEnv.hh>
#include <XrdCl/XrdClFile.hh>

#ifdef HAVE_XROOTD_PRIVATE
#include <XrdCl/XrdClPostMaster.hh>
#endif

#define CKS_TYPE "adler32"
#define RECONNECT_DELAY (1000 * 60 * 15) // reconnect every 15 minutes on error

namespace bfs = boost::filesystem;
namespace chrono = std::chrono;
using namespace ntof::rfm;

const std::string XRootArchiver::NAME("XRootArchiver");
static RWLock s_connPoolLock;

static uint32_t getRemoteChecksum(const std::string &url)
{
    uint32_t cksum = 0;
    XrdCl::Buffer arg;
    XrdCl::Buffer *response = nullptr;
    arg.FromString(url);

    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());
    XrdCl::XRootDStatus status = fs.Query(XrdCl::QueryCode::Checksum, arg,
                                          response);
    lock.unlock();
    if (status.IsOK() && response)
    {
        std::istringstream iss(response->ToString());
        std::string type;
        iss >> type;
        iss >> std::hex >> cksum;

        // el::base::Writer &writer(LOG(TRACE));
        // ntof::log::FlagsGuard flags(writer);
        // writer << "remote checksum: type:" << type << " cksum: " << std::hex
        //        << cksum;
        delete response;
    }
    return cksum;
}

/**
 * @brief small unique_ptr like class for XrdCl::StatInfo
 */
struct StatInfoPtr
{
    StatInfoPtr() : ptr(nullptr) {}
    ~StatInfoPtr()
    {
        if (ptr)
            delete ptr;
    }

    void clear()
    {
        if (ptr)
            delete ptr;
        ptr = nullptr;
    }

    XrdCl::StatInfo *ptr;
};

class XRootFile : public Archiver::File
{
public:
    explicit XRootFile(XRootArchiver &archiver, const std::string &filename);

    ~XRootFile();

    void write(uint64_t offset, uint32_t size, const void *buffer) override;

    void close() override;

    XRootArchiver &m_archiver;
    XrdCl::File m_file;
    std::string m_url;
    uint32_t m_cksum;
};

XRootFile::XRootFile(XRootArchiver &archiver, const std::string &filename) :
    m_archiver(archiver), m_cksum(::adler32(0, 0, 0))
{
    Config &c = Config::instance();
    {
        std::ostringstream oss;
        oss << filename << "?svcClass=" << c.getSvcClass()
            << "&cks.type=" << CKS_TYPE;
        if (!c.getCastorOptions().empty())
        {
            oss << "&" << c.getCastorOptions();
        }
        m_url = oss.str();
    }

    {
        std::unique_lock<RWLock> lock(s_connPoolLock);
        LOG(TRACE) << "[XRootArchiver]: Opening XrdCl::File at " << m_url;
        /* it seems that we're only checking write return code */
        m_file.Open("root://" + c.getCastorNode() + "/" + m_url,
                    XrdCl::OpenFlags::Write | XrdCl::OpenFlags::Delete);
    }
}

XRootFile::~XRootFile()
{
    if (m_file.IsOpen())
        m_file.Close();
}

void XRootFile::write(uint64_t offset, uint32_t size, const void *buffer)
{
    if (size == 0)
        return; // adler32 gets reset on zero writes

    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::XRootDStatus status = m_file.Write(offset, size, buffer);
    lock.unlock();
    if (!status.IsOK())
    {
        std::ostringstream oss;
        oss << "Unable to write XRoot file: \"" << status.ToStr() << "\"";
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        m_archiver.reportError();
        throw Archiver::Exception(oss.str());
    }
    m_cksum = ::adler32(m_cksum, reinterpret_cast<const uint8_t *>(buffer),
                        size);
}

void XRootFile::close()
{
    if (!m_file.IsOpen())
        return;

    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::XRootDStatus status = m_file.Close();
    lock.unlock();
    if (!status.IsOK())
    {
        std::ostringstream oss;
        oss << "Unable to close XRoot file " << status.GetErrorMessage();
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        m_archiver.reportError();
        throw Archiver::Exception(oss.str());
    }
    if (m_cksum)
    {
        LOG(TRACE) << "validating checksum";
        uint32_t remoteSum = getRemoteChecksum(m_url);

        if (m_cksum != remoteSum)
        {
            std::ostringstream oss;
            oss << "Checksum mismatch for file: " << m_url << " (" << std::hex
                << m_cksum << " != " << std::hex << remoteSum << ")";
            LOG(ERROR) << "[XRootArchiver]: " << oss.str();
            throw Archiver::Exception(oss.str());
        }
        else
        {
            LOG(INFO)
                << "[XRootArchiver]: Checksum validated for file: " << m_url;
        }
    }
}

XRootArchiver::XRootArchiver() : m_lastReconnect(chrono::steady_clock::now()) {}

const std::string &XRootArchiver::name() const
{
    return XRootArchiver::NAME;
}

XRootArchiver::File::Shared XRootArchiver::open(const bfs::path &file)
{
    return XRootArchiver::File::Shared(new XRootFile(*this, file.string()));
}

bool XRootArchiver::isMigrated(const bfs::path &file)
{
    bool isMigrated = false;
    const std::string path = file.string();

    LOG(DEBUG) << "[XRootArchiver]: testing stager state for " << path;
    StatInfoPtr info;

    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());
    XrdCl::XRootDStatus status = fs.Stat(path, info.ptr);
    lock.unlock();
    if (status.IsOK() && info.ptr)
    {
        // file exists and at this point is copied/migrated
        isMigrated = true;
    }

    if (isMigrated)
    {
        LOG(DEBUG) << "[XRootArchiver]: file " << path << " is migrated";
    }
    return isMigrated;
}

XRootArchiver::FileInfo XRootArchiver::getInfo(const bfs::path &file)
{
    std::string path = file.string();

    if (path[path.length() - 1] == '/')
    {
        path = path.substr(0, path.length() - 1);
    }
    LOG(TRACE)
        << "[XRootArchiver]: getting CASTOR file information for " << path;

    StatInfoPtr info;
    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());
    XrdCl::XRootDStatus status = fs.Stat(path, info.ptr);
    lock.unlock();

    if (!status.IsOK() || !info.ptr)
        return XRootArchiver::FileInfo();
    else
        return XRootArchiver::FileInfo(true, info.ptr->GetSize());
}

void XRootArchiver::createDir(const bfs::path &file)
{
    std::string path = file.string();
    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());

    // Remove trailing /
    if (!path.empty() && path[path.length() - 1] == '/')
    {
        path = path.substr(0, path.length() - 1);
    }

    if (path.empty() ||
        ((path[0] != '/') && (path.find(":/") == std::string::npos)))
    {
        std::ostringstream oss;
        oss << "cannot create directory " << path << ": invalid path";
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        reportError();
        throw Archiver::Exception(oss.str());
    }
    StatInfoPtr info;
    XrdCl::XRootDStatus status = fs.Stat(path, info.ptr);
    if (status.IsOK() && info.ptr)
    {
        if (!info.ptr->TestFlags(XrdCl::StatInfo::IsDir))
        {
            // It's NOT a folder
            std::ostringstream oss;
            oss << "cannot create directory " << path
                << ": path already exists!";
            LOG(ERROR) << "[XRootArchiver]: " << oss.str();
            lock.unlock();
            reportError();
            throw Archiver::Exception(oss.str());
        }
        else
        {
            LOG(TRACE)
                << "[XRootArchiver]: folder " << path << " already exists";
            // already exists and is a folder
            return;
        }
    }
    info.clear();

    LOG(TRACE) << "[XRootArchiver]: creating CASTOR directory: " << path;
    status = fs.MkDir(path, XrdCl::MkDirFlags::MakePath,
                      XrdCl::Access::UR | XrdCl::Access::UW | XrdCl::Access::UX |
                          XrdCl::Access::GR | XrdCl::Access::GX |
                          XrdCl::Access::OR | XrdCl::Access::OX);
    lock.unlock();
    if (!status.IsOK())
    {
        std::ostringstream oss;
        oss << "cannot create directory " << path << ": "
            << status.GetErrorMessage();
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        reportError();
        throw Archiver::Exception(oss.str());
    }
}

void XRootArchiver::deleteFile(const bfs::path &file)
{
    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());
    XrdCl::XRootDStatus status = fs.Rm(file.string());
    lock.unlock();
    if (!status.IsOK())
    {
        std::ostringstream oss;
        oss << "Unable to delete CASTOR file " << file << ": "
            << status.GetErrorMessage();
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        reportError();
        throw Archiver::Exception(oss.str());
    }
}

void XRootArchiver::renameFile(const bfs::path &file, const bfs::path &newFile)
{
    const std::string oldPath = file.string();
    const std::string newPath = newFile.string();

    LOG(TRACE) << "[XRootArchiver]: renaming CASTOR file from " << oldPath
               << " to " << newPath;
    std::unique_lock<RWLock> lock(s_connPoolLock);
    XrdCl::FileSystem fs(Config::instance().getCastorNode());
    XrdCl::XRootDStatus status = fs.Mv(oldPath, newPath);
    lock.unlock();
    if (!status.IsOK())
    {
        std::ostringstream oss;
        oss << "Unable to rename CASTOR file " << oldPath << " to " << newPath
            << ":" << status.GetErrorMessage();
        LOG(ERROR) << "[XRootArchiver]: " << oss.str();
        reportError();
        throw Archiver::Exception(oss.str());
    }
}

void XRootArchiver::reportError()
{
    std::lock_guard<std::mutex> lock(m_lock);
    const chrono::milliseconds delay =
        chrono::duration_cast<chrono::milliseconds>(
            chrono::steady_clock::now() - m_lastReconnect);

    if (delay.count() >= RECONNECT_DELAY)
    {
        /* ensure nothing else is running */
        s_connPoolLock.lock(true);
        try
        {
            XrdCl::PostMaster *postMaster = XrdCl::DefaultEnv::GetPostMaster();
            if (postMaster)
            {
#ifdef HAVE_XROOTD_PRIVATE
                LOG(WARNING) << "[XRootArchiver]: Reconnecting server...";
                postMaster->ForceDisconnect(Config::instance().getCastorNode());
#else
#warning Missing xrootd private headers
                LOG(WARNING)
                    << "[XRootArchiver]: Can't reconnect server (missing "
                       "xrootd-private headers)...";
#endif
            }
            m_lastReconnect = chrono::steady_clock::now();
        }
        catch (...)
        {}

        s_connPoolLock.unlock();
    }
    else
    {
        LOG(TRACE) << "[XRootArchiver]: Too early to reconnect";
    }
}
