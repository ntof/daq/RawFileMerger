/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-04T15:42:55+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef LOCALARCHIVER_HPP__
#define LOCALARCHIVER_HPP__

#include <fstream>

#include "Archiver.hpp"

namespace ntof {
namespace rfm {

class LocalArchiver : public Archiver
{
public:
    LocalArchiver() = default;

    const std::string &name() const override;

    File::Shared open(const boost::filesystem::path &file) override;

    bool isMigrated(const boost::filesystem::path &file) override;

    FileInfo getInfo(const boost::filesystem::path &file) override;

    void createDir(const boost::filesystem::path &file) override;

    void deleteFile(const boost::filesystem::path &file) override;

    void renameFile(const boost::filesystem::path &file,
                    const boost::filesystem::path &newFile) override;

    static const std::string NAME;

protected:
    std::mutex m_lock;
};

class LocalFile : public Archiver::File
{
public:
    explicit LocalFile(LocalArchiver &archiver, const std::string &filename);
    ~LocalFile();

    void write(uint64_t offset, uint32_t size, const void *buffer) override;

    void close() override;

    LocalArchiver &m_archiver;
    std::string m_filename;
    std::streampos m_pos;
    std::ofstream m_file;
};

} // namespace rfm
} // namespace ntof

#endif
