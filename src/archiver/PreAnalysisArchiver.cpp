/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T14:04:59+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "PreAnalysisArchiver.hpp"

#include <regex>
#include <sstream>

#include <NTOFLogging.hpp>
#include <spawn.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "Config.h"
#include "FileUtils.hpp"
#include "RFMException.hpp"
#include "RFMUtils.hpp"
#include "Regex.hpp"

using namespace ntof::rfm;

const std::string PreAnalysisArchiver::NAME("PreAnalysisArchiver");

static const Regex s_fileNameRe("^run([[:digit:]]+)_([[:digit:]]+)_",
                                Regex::Extended);

class PreAnalysisFile : public LocalFile
{
public:
    explicit PreAnalysisFile(LocalArchiver &archiver,
                             const std::string &filename);

    void close() override;

    void runScript();
};

PreAnalysisFile::PreAnalysisFile(LocalArchiver &archiver,
                                 const std::string &filename) :
    LocalFile(archiver, filename)
{}

void PreAnalysisFile::close()
{
    LocalFile::close();
    runScript();
}

void PreAnalysisFile::runScript()
{
    const Config::PreAnalysis *cfg = Config::instance().getPreAnalysisConfig();

    if (!cfg)
    {
        LOG(ERROR) << "[PreAnalysisArchiver]: not configured";
        return;
    }
    LOG(INFO) << "[PreAnalysisArchiver]: running analysis on: " << m_filename;
    posix_spawn_file_actions_t actions;
    pid_t pid;

    posix_spawn_file_actions_init(&actions);
    posix_spawn_file_actions_addclose(&actions, STDIN_FILENO);

    const char *argv[] = {cfg->cmd.c_str(), m_filename.c_str(), nullptr};
    int ret = ::posix_spawn(&pid, cfg->cmd.c_str(), &actions, nullptr,
                            const_cast<char **>(argv), environ);
    posix_spawn_file_actions_destroy(&actions);
    if (ret != 0)
    {
        LOG(ERROR)
            << "[PreAnalysisArchiver]: failed to start process: " << cfg->cmd
            << " errno:" << strerror(errno);
    }
    else
    {
        int status;
        ::waitpid(pid, &status, 0);
        std::string msg;
        if (!WIFEXITED(status))
        {
            msg = "analysis process crashed";
            LOG(ERROR) << "[PreAnalysisArchiver]: " << msg;
        }
        else if (WEXITSTATUS(status) != 0)
        {
            msg = "analysis process exited with code: ";
            msg += std::to_string(WEXITSTATUS(status));
            LOG(ERROR) << "[PreAnalysisArchiver]: " << msg;
        }
        if (!msg.empty() && cfg->checkErrorCode)
        {
            throw RFMException(msg);
        }
    }
}

PreAnalysisArchiver::PreAnalysisArchiver() : m_runNumber(0), m_fileNumber(-1) {}

const std::string &PreAnalysisArchiver::name() const
{
    return NAME;
}

Archiver::File::Shared PreAnalysisArchiver::open(
    const boost::filesystem::path &file)
{
    return Archiver::File::Shared(new PreAnalysisFile(*this, file.string()));
}

Archiver::FileInfo PreAnalysisArchiver::getInfo(
    const boost::filesystem::path &file)
{
    Archiver::FileInfo info(LocalArchiver::getInfo(file));
    const Config::PreAnalysis *cfg = Config::instance().getPreAnalysisConfig();

    try
    {
        if (endsWith(file.string(), ".idx.finished"))
        {
            LOG(INFO)
                << "[PreAnalysisArchiver]: ignoring index: " << file.string();
            info.ignore = true;
            return info;
        }

        std::vector<std::string> match;
        match.reserve(3);
        if (!s_fileNameRe.match(file.filename().string(), match))
        {
            LOG(ERROR) << "[PreAnalysisArchiver]: failed to extract "
                          "runNumber/fileNumber";
            info.ignore = true;
            return info;
        }
        RunNumber runNumber = std::stoul(match[1]);
        FileNumber fileNumber = std::stoll(match[2]);

        if (!cfg)
        {
            LOG(ERROR) << "[PreAnalysisArchiver]: not configured";
            throw std::invalid_argument("not configured");
        }
        if (cfg->onlyNew)
        {
            std::lock_guard<std::mutex> lock(m_lock);
            if (runNumber < m_runNumber)
            {
                LOG(WARNING)
                    << "[PreAnalysisArchiver]: ignoring old run file runNumber:"
                    << runNumber << " current:" << m_runNumber;
                info.ignore = true;
            }
            else if ((runNumber == m_runNumber) && (fileNumber < m_fileNumber))
            {
                LOG(WARNING)
                    << "[PreAnalysisArchiver]: ignoring old file fileNumber:"
                    << fileNumber << " current:" << m_fileNumber;
                info.ignore = true;
            }
            else
            {
                m_runNumber = runNumber;
                m_fileNumber = fileNumber;
            }
        }
        if ((cfg->skipCount != 0) && ((fileNumber % (cfg->skipCount + 1)) != 0))
        {
            LOG(INFO)
                << "[PreAnalysisArchiver]: ignoring file: " << file.string();
            info.ignore = true;
        }
    }
    catch (...)
    {
        LOG(ERROR) << "[PreAnalysisArchiver]: failed to extract "
                      "runNumber/sequenceNumber file:"
                   << file.string();
        info.ignore = true;
    }
    return info;
}

void PreAnalysisArchiver::finishFile(const boost::filesystem::path &file)
{
    LOG(INFO) << "[PreAnalysisArchiver]: removing file: " << file.string();
    FileUtils::deleteFile(file.string());
}

void PreAnalysisArchiver::finishRun(const boost::filesystem::path &file)
{
    if (Config::instance().getRemoteSubdirs() != Config::RemoteSubdirs::NONE)
    {
        LOG(INFO) << "[PreAnalysisArchiver]: removing run directory: "
                  << file.string();
        FileUtils::deleteDirectory(file.string());
    }
}
