/*
 * RFMUtils.cpp
 *
 *  Created on: Nov 3, 2015
 *      Author: mdonze
 */

#include "RFMUtils.hpp"

#include <cerrno>

#include <NTOFLogging.hpp>

using namespace ntof::rfm;

TransferRate::TransferRate() : transferred(0), rate(0) {}

void TransferRate::update(std::size_t newTr)
{
    if (transferred == 0 || newTr < transferred)
    {
        rate = 0;
        transferred = newTr;
        m_last = std::chrono::steady_clock::now();
    }
    else
    {
        std::chrono::time_point<std::chrono::steady_clock> now =
            std::chrono::steady_clock::now();
        std::chrono::duration<double> dur = now - m_last;
        if (dur.count() != 0)
        {
            rate = (newTr - transferred) / dur.count();
            m_last = now;
        }
        transferred = newTr;
    }
}

RWLock::RWLock()
{
    if (pthread_rwlock_init(&m_lock, nullptr) != 0)
    {
        LOG(ERROR) << "[RWLock] init failure: " << std::strerror(errno);
    }
}

RWLock::~RWLock()
{
    if (pthread_rwlock_destroy(&m_lock) != 0)
    {
        LOG(ERROR) << "[RWLock] destroy failure: " << std::strerror(errno);
    }
}

bool RWLock::lock(bool write)
{
    if (write)
    {
        if (pthread_rwlock_wrlock(&m_lock) != 0)
        {
            LOG(ERROR) << "[RWLock] wrlock failure: " << std::strerror(errno);
            return false;
        }
    }
    else if (pthread_rwlock_rdlock(&m_lock) != 0)
    {
        LOG(ERROR) << "[RWLock] rdlock failure: " << std::strerror(errno);
        return false;
    }
    return true;
}

void RWLock::unlock()
{
    if (pthread_rwlock_unlock(&m_lock) != 0)
    {
        LOG(ERROR) << "[RWLock] unlock failure: " << std::strerror(errno);
    }
}

vstreambuf::vstreambuf(std::vector<char> &vector)
{
    setg(vector.data(), vector.data(), vector.data() + vector.size());
    setp(vector.data(), vector.data() + vector.size());
}

std::streampos vstreambuf::seekoff(std::streamoff off,
                                   std::ios_base::seekdir way,
                                   std::ios_base::openmode which)
{
    pos_type ret = -1;
    off_type offi = off;
    off_type offo = offi;

    if (way == std::ios_base::cur)
    {
        offi += gptr() - eback();
        offo += pptr() - pbase();
    }
    else if (way == std::ios_base::end)
    {
        offi += egptr() - eback();
        offo += epptr() - pbase();
    }
    if ((std::ios_base::in & which) && (offi >= 0) &&
        (egptr() - eback() >= offi))
    {
        setg(eback(), eback() + offi, egptr());
        ret = pos_type(offi);
    }
    if ((std::ios_base::out & which) && (offo >= 0) &&
        (epptr() - pbase() >= offo))
    {
        pbump(offo);
        ret = pos_type(offo);
    }
    return ret;
}

std::streampos vstreambuf::seekpos(std::streampos pos,
                                   std::ios_base::openmode which)
{
    return seekoff(off_type(pos), std::ios_base::beg, which);
}

bool ntof::rfm::endsWith(const std::string &str, const std::string &needle)
{
    return (str.size() >= needle.size() &&
            str.compare(str.size() - needle.size(), str.size(), needle) == 0);
}
