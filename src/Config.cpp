//
// Created by matteof on 01/07/2020.
//

#include "Config.h"

#include <algorithm>

#include <NTOFException.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "archiver/PreAnalysisArchiver.hpp"
#include "archiver/XRootArchiver.hpp"

#include <Singleton.hxx>

using namespace ntof::rfm;

template class ntof::utils::Singleton<Config>;

const std::string Config::configFile = "/etc/ntof/rfmerger.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";
const std::string Config::defaultDatabaseName = "/var/lib/RawFileMerger/"
                                                "db.sqlite";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile), listen_(ListeningMode::NONE), deleterEnabled_(true)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        LOG(ERROR) << "Unable to open and parse configuration file!";
        throw NTOFException("Unable to open and parse configuration file!",
                            __FILE__, __LINE__);
    }

    // Parse the config file
    const pugi::xml_node &root = doc.first_child();

    // CASTOR paths
    const pugi::xml_node &paths = root.child("paths");
    // Load local file path (for run and event files)
    paths_.local = getPath("Local", paths.child("local"));
    // Load remote CASTOR file path
    paths_.remote = getPath("Remote", paths.child("remote"));
    fromString(paths.child("remote").attribute("subdirs").as_string("FULL"),
               m_remoteSubdirs);
    // Load mount point
    paths_.mount = paths.child("mount").attribute("path").as_string("/mnt");
    paths_.database = paths.child("database")
                          .attribute("path")
                          .as_string(defaultDatabaseName.c_str());

    // Gets CASTOR SVC node and service class
    const pugi::xml_node &castor = root.child("castor");

    archiverName_ =
        root.child("archiver").attribute("name").as_string("XRootArchiver");
    castorNode_ = castor.attribute("node").as_string();
    if (castorNode_.empty() &&
        (archiverName_ == XRootArchiver::NAME ||
         archiverName_ == "CastorArchiver"))
    {
        throw NTOFException("CASTOR cluster node cannot be empty!", __FILE__,
                            __LINE__);
    }

    svcClass_ = castor.attribute("svcclass").as_string("default");
    castorOptions_ = castor.attribute("options").as_string();

    // CASTOR File limits to reach
    const pugi::xml_node &limitNode = root.child("limits");
    minTotalSize_ = parseSize(limitNode.attribute("size").as_string("2G"));
    maxEvents_ = limitNode.attribute("count").as_int(50);
    m_expiry = limitNode.attribute("expiry").as_int(60 * 60 * 24 *
                                                    31); // 31 days

    // DIM service prefix and history count
    const pugi::xml_node &dimNode = root.child("DIM");
    dimPrefix_ = dimNode.attribute("prefix").as_string("CASTOR");
    dimHistoryCount_ = dimNode.attribute("history").as_int(10);
    dimErrorsCount_ = dimNode.attribute("errors").as_uint(20);

    // Thread pool size
    poolSize_ = root.child("pool").attribute("size").as_int(20);
    // Buffer size
    bufferSize_ = root.child("buffer").attribute("size").as_ullong(0);

    // CASTOR replace strategy
    if (!fromString(root.child("replace").attribute("strategy").as_string(""),
                    replaceSty_))
    {
        LOG(ERROR) << "Unsupported replace strategy name!";
        throw NTOFException(
            "Replace strategy must be RENAME, DELETE or IGNORE!", __FILE__,
            __LINE__);
    }

    fromString(root.child("listen").attribute("value").as_string(""), listen_);
    deleterEnabled_ = root.child("deleter").attribute("enabled").as_bool(true);

    if (archiverName_ == PreAnalysisArchiver::NAME)
    {
        m_preAnalysis.reset(new PreAnalysis);
        m_preAnalysis->cmd =
            root.child("archiver").attribute("cmd").as_string("");
        m_preAnalysis->skipCount =
            root.child("archiver").attribute("skipCount").as_int(0);
        m_preAnalysis->checkErrorCode =
            root.child("archiver").attribute("checkErrorCode").as_bool(true);
        m_preAnalysis->onlyNew =
            root.child("archiver").attribute("onlyNew").as_bool(true);
    }
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getDimPrefix() const
{
    return dimPrefix_;
}

uint32_t Config::getDimHistoryCount() const
{
    return dimHistoryCount_;
}

uint32_t Config::getDimErrorsCount() const
{
    return dimErrorsCount_;
}

const std::string &Config::getCastorNode() const
{
    return castorNode_;
}

const std::string &Config::getSvcClass() const
{
    return svcClass_;
}

uint32_t Config::getMaxEvents() const
{
    return maxEvents_;
}

uint64_t Config::getMinTotalSize() const
{
    return minTotalSize_;
}

Config::ReplaceStrategy Config::getReplaceStrategy() const
{
    return replaceSty_;
}

bool Config::isDeleterEnabled() const
{
    return deleterEnabled_;
}

void Config::setDeleterEnabled(bool value)
{
    deleterEnabled_ = value;
}

const std::string &Config::getArchiverName() const
{
    return archiverName_;
}

const Config::Paths &Config::getPaths() const
{
    return paths_;
}

uint32_t Config::getPoolSize() const
{
    return poolSize_;
}
uint64_t Config::getBufferSize() const
{
    return bufferSize_;
}

/**
 * Helper function to extract the path attribute from a node
 * @param pathNode
 * @return
 */
std::string Config::getPath(const std::string &name,
                            const pugi::xml_node &pathNode) const
{
    if (pathNode)
    {
        std::string ret = pathNode.attribute("path").as_string();
        if (ret.empty())
        {
            std::ostringstream oss;
            oss << name << " path cannot be empty!";
            LOG(ERROR) << oss.str();
            throw NTOFException(oss.str(), __FILE__, __LINE__);
        }
        if (ret[ret.length() - 1] == '/')
        {
            ret = ret.substr(0, ret.length() - 1);
        }
        return ret;
    }
    else
    {
        std::ostringstream oss;
        oss << name << " path node is not valid!";
        LOG(ERROR) << oss.str();
        throw NTOFException(oss.str(), __FILE__, __LINE__);
    }
}

/**
 * Gets how the software will behave if CASTOR file is already present
 * @return
 */
/**
 * Parse a string giving a size
 * @param sizeStr
 * @return
 */
uint64_t Config::parseSize(const std::string &sizeStr) const
{
    int size = 0;
    char unit[64];
    int res = sscanf(sizeStr.c_str(), "%d%63s", &size, unit);
    if (res == 1)
    {
        LOG(INFO) << "Size unit not specified, using bytes";
        // No unit specified, it's only bytes
        return ((uint64_t) size);
    }
    else
    {
        std::string unitStr = unit;
        switch (unitStr[0])
        {
        case 'K':
        case 'k':
            // This is kilobytes
            return ((uint64_t) size) * 1024;
        case 'M':
        case 'm':
            // This is megabytes
            return ((uint64_t) size) * 1048576;
        case 'G':
        case 'g':
            // This is gigabytes
            return ((uint64_t) size) * 1073741824;
        case 'T':
        case 't':
            // This is terabytes?
            return ((uint64_t) size) * 1099511627776U;
        default: return ((uint64_t) size);
        }
    }
}

std::string Config::toString(ListeningMode mode)
{
    switch (mode)
    {
    case ListeningMode::DAQ: return "DAQ";
    case ListeningMode::EACS: return "EACS";
    case ListeningMode::NONE: return "NONE";
    default: return "UNKNOWN";
    }
}

std::string Config::toString(ReplaceStrategy strategy)
{
    switch (strategy)
    {
    case ReplaceStrategy::RENAME: return "RENAME";
    case ReplaceStrategy::DELETE: return "DELETE";
    case ReplaceStrategy::IGNORE: return "IGNORE";
    default: return "UNKNOWN";
    }
}

std::string Config::toString(RemoteSubdirs rsub)
{
    switch (rsub)
    {
    case RemoteSubdirs::NONE: return "NONE";
    case RemoteSubdirs::FULL: return "FULL";
    case RemoteSubdirs::RUN: return "RUN";
    default: return "UNKNOWN";
    }
}

bool Config::fromString(const std::string &sty, ReplaceStrategy &out)
{
    std::string usty(sty);
    std::transform(usty.begin(), usty.end(), usty.begin(), ::toupper);

    if (usty == "RENAME")
        out = ReplaceStrategy::RENAME;
    else if (usty == "DELETE")
        out = ReplaceStrategy::DELETE;
    else if (usty == "IGNORE")
        out = ReplaceStrategy::IGNORE;
    else
    {
        out = ReplaceStrategy::UNKNOWN;
        return false;
    }
    return true;
}

bool Config::fromString(const std::string &sty, ListeningMode &out)
{
    std::string usty(sty);
    std::transform(usty.begin(), usty.end(), usty.begin(), ::toupper);

    if (usty == "DAQ")
        out = ListeningMode::DAQ;
    else if (usty == "EACS")
        out = ListeningMode::EACS;
    else
    {
        out = ListeningMode::NONE;
        return false;
    }
    return true;
}

bool Config::fromString(const std::string &str, RemoteSubdirs &out)
{
    std::string ustr(str);
    std::transform(ustr.begin(), ustr.end(), ustr.begin(), ::toupper);

    if (ustr == "NONE")
        out = RemoteSubdirs::NONE;
    else if (ustr == "FULL")
        out = RemoteSubdirs::FULL;
    else if (ustr == "RUN")
        out = RemoteSubdirs::RUN;
    else
    {
        out = RemoteSubdirs::FULL; // legacy mode
        return false;
    }

    return true;
}
