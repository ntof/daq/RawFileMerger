/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-14T16:49:28+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMDAQLISTENER_HPP__
#define RFMDAQLISTENER_HPP__

#include <memory>
#include <vector>

#include <DIMXMLInfo.h>

#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class RFMManager;

/**
 * @brief listens for DAQ events and inject it in runs
 */
class DaqListener
{
public:
    explicit DaqListener(RFMManager &manager);
    ~DaqListener();

protected:
    void listen();

    RFMManager &m_mgr;
    std::vector<std::shared_ptr<ntof::dim::DIMXMLInfo>> m_info;
};

} // namespace rfm
} // namespace ntof
#endif
