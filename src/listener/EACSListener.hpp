/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-14T16:35:10+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef EACSLISTENER_HPP__
#define EACSLISTENER_HPP__

#include <DIMDataSetClient.h>

#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class RFMManager;

/**
 * @brief listen to EACS/Validation events
 */
class EACSListener
{
public:
    explicit EACSListener(RFMManager &manager);
    ~EACSListener();

    enum ValidationParams
    {
        RUN_NUMBER = 0,
        EVENT_NUMBER,
        VALIDATED_NUMBER,
        FILE_SIZE
    };

protected:
    void onEvent(const dim::DIMData::List &data);
    bool checkRun(RunInfo::RunNumber runNumber);

    RFMManager &m_mgr;
    std::unique_ptr<dim::DIMDataSetClient> m_info;
};

} // namespace rfm
} // namespace ntof

#endif
