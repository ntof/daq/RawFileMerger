/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-14T16:39:17+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "EACSListener.hpp"

#include <limits>

#include <NTOFLogging.hpp>

#include "Config.h"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "data/DaqInfo.hpp"
#include "data/FileEvent.hpp"

using namespace ntof::rfm;
using namespace ntof::dim;

EACSListener::EACSListener(RFMManager &mgr) :
    m_mgr(mgr), m_info(new DIMDataSetClient)
{
    m_info->dataSignal.connect(
        [this](DIMDataSetClient &client) { onEvent(client.getLatestData()); });
    m_info->subscribe("EACS/Validation");
}

EACSListener::~EACSListener()
{
    m_info.reset();
}

void EACSListener::onEvent(const DIMData::List &list)
{
    RunInfo::RunNumber runNumber = RunInfo::InvalidRunNumber;
    FileEvent::EventNumber eventNumber =
        std::numeric_limits<FileEvent::EventNumber>::max();
    FileEvent::SequenceNumber sequenceNumber =
        std::numeric_limits<FileEvent::SequenceNumber>::max();
    uint64_t fileSize = 0;

    for (const DIMData &data : list)
    {
        switch (data.getIndex())
        {
        case RUN_NUMBER: runNumber = data.getValue<uint32_t>(); break;
        case EVENT_NUMBER: eventNumber = data.getValue<uint32_t>(); break;
        case VALIDATED_NUMBER:
            sequenceNumber = data.getValue<uint32_t>();
            break;
        case FILE_SIZE: fileSize = data.getValue<uint64_t>(); break;
        }
    }

    if ((runNumber == RunInfo::InvalidRunNumber) || (runNumber == 0))
        LOG(ERROR) << "[EACSListener]: invalid event received: "
                   << "invalid runNumber";
    else if ((eventNumber ==
              std::numeric_limits<FileEvent::EventNumber>::max()) ||
             (sequenceNumber ==
              std::numeric_limits<FileEvent::SequenceNumber>::max()))
        LOG(ERROR) << "[EACSListener]: invalid event received: "
                   << "invalid event or sequence number";
    else if (fileSize == 0)
        LOG(ERROR) << "[EACSListener]: invalid event received: "
                   << "invalid fileSize";
    else if (checkRun(runNumber))
    {
        m_mgr.addEvent(runNumber, sequenceNumber, eventNumber, fileSize);
    }
}

bool EACSListener::checkRun(RunInfo::RunNumber runNumber)
{
    const RunInfo::Shared current = m_mgr.currentRun();
    if (!current || current->runNumber() != runNumber)
    {
        RunInfo::Shared run = RFMFactory::instance().createRun(
            runNumber, Config::instance().getExperimentalArea(),
            std::time(nullptr), DaqInfo::CrateIdList(), std::string());
        if (!run)
        {
            LOG(ERROR) << "[EACSListener]: failed to create run";
            return false;
        }
        run->setApproved(true);
        run->save();
        m_mgr.setCurrentRun(*run);
    }
    return true;
}
