/*
 * FileDeleter.cpp
 *
 *  Created on: Nov 5, 2015
 *      Author: mdonze
 */

#include "FileDeleter.hpp"

#include <chrono>
#include <iostream>

#include <easylogging++.h>

#include "Archiver.hpp"
#include "Config.h"
#include "FileUtils.hpp"
#include "IndexerTask.h"
#include "Paths.hpp"
#include "RFMFactory.hpp"
#include "data/FileStats.hpp"
#include "data/RunInfo.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

#define REFRESH_RATE 60000

FileDeleter::FileDeleter() : Thread("FileDeleter"), m_prevError(false)
{
    setInterval(REFRESH_RATE);
}

void FileDeleter::setInterval(uint32_t ms)
{
    Thread::setInterval(std::chrono::milliseconds(ms));
}

void FileDeleter::add(const MergedFile &file)
{
    LOG(DEBUG) << "[FileDeleter]: adding " << file.fileName() << " to deleter";
    RunInfo::Shared run = RFMFactory::instance().getRun(file.runNumber());

    std::lock_guard<std::mutex> lock(m_lock);
    m_files[uid(file)] = file.shared();
    if (run)
    {
        m_runs[run->runNumber()] = run;
    }
    else
    {
        LOG(ERROR)
            << "[FileDeleter]: failed to retrieve run for " << file.fileName();
    }
}

void FileDeleter::add(const RunInfo &run)
{
    LOG(DEBUG)
        << "[FileDeleter]: adding run " << run.runNumber() << " to deleter";

    std::lock_guard<std::mutex> lock(m_lock);
    m_runs[run.runNumber()] = run.shared();
}

void FileDeleter::thread_enter()
{
    m_prevError = false;
}

void FileDeleter::thread_func()
{
    std::unique_lock<std::mutex> lock(m_lock);
    RunMap runs = m_runs;
    MergedFileMap files = m_files;
    lock.unlock();

    LOG(INFO) << "[FileDeleter]: Deleter is running...";

    try
    {
        VLOG(1) << "Deleter is checking for completed file...";
        checkFiles(files, runs);
        checkRuns(runs);
        removeOldRuns();

        if (m_prevError)
        {
            m_prevError = false;
            errorSignal("Deleter", std::string()); /* error is over */
        }
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "Deleter exception : " << ex.what();
        errorSignal("Deleter", ex.what());
        m_prevError = true;
    }
    catch (...)
    {
        LOG(ERROR) << "Unknown exception occurred in deleter";
        errorSignal("Deleter", "Unknown exception occurred in deleter");
        m_prevError = true;
    }
}

void FileDeleter::checkFiles(FileDeleter::MergedFileMap &files,
                             FileDeleter::RunMap &runs)
{
    for (MergedFileMap::value_type &it : files)
    {
        MergedFile::Shared file(it.second);
        RunInfo::Shared run(runs[file->runNumber()]);

        if (!run)
            throw RFMException("failed to find run associated with: " +
                               file->fileName());
        else if ((file->status() != MergedFile::COPIED) &&
                 (file->status() != MergedFile::MIGRATED))
        {
            LOG(WARNING) << "[FileDeleter]: can't remove a file in "
                         << MergedFile::toString(file->status())
                         << " state: " << file->fileName();
            std::lock_guard<std::mutex> lock(m_lock);
            m_files.erase(uid(*file));
            continue;
        }

        bfs::path filePath = Paths::getRemoteFilePath(*run, *file);
        Archiver::FileInfo info = Archiver::instance().getInfo(filePath);
        if (!info || info.fileSize == 0)
        {
            // File is empty or not existing
            std::ostringstream errMsg;
            errMsg << "remote file " << filePath.string()
                   << " is not present or empty";
            LOG(ERROR) << "[FileDeleter]: " << errMsg.str();
            // Try to re-merge the file
            fileErrorSignal(*file, errMsg.str());
            std::lock_guard<std::mutex> lock(m_lock);
            m_files.erase(uid(*file));
        }
        else if (Archiver::instance().isMigrated(filePath))
        {
            LOG(INFO) << "[FileDeleter]: file " << filePath.string()
                      << " is staged, deleting local files";
            Archiver::instance().finishFile(filePath);
            removeRawFiles(*run, *file);
            file->setStatus(MergedFile::MIGRATED);
            file->save();
        }
    }
}

void FileDeleter::checkRuns(FileDeleter::RunMap &runs)
{
    for (RunMap::value_type &it : runs)
    {
        RunInfo::Shared &run(it.second);
        if (!run->hasStopDate())
        {
            LOG(INFO) << "[FileDeleter]: run " << run->runNumber()
                      << " still in progress, skipping";
            continue;
        }
        if (run->isApproved())
        {
            run->createIndex(); // ensure index has been created
            run->updateStats();
            FileStats stats = run->fileStats();
            std::size_t total = 0;
            for (FileStats::value_type &stat : stats)
                total += stat.second;

            if (stats[MergedFile::MIGRATED] == total)
            {
                LOG(INFO) << "[FileDeleter]: run " << run->runNumber()
                          << " indexed and migrated. Deleting related "
                             "folders.";

                Archiver::instance().finishRun(Paths::getRemotePath(*run));
                removeRunFiles(*run);
            }
        }
        else if (run->expiryDate() == RunInfo::Expired)
        {
            LOG(INFO) << "[FileDeleter]: run expired: " << run->runNumber();
            // no Archiver finish needed, run was never approved
            for (MergedFile::Shared file : run->getFiles())
                removeRawFiles(*run, *file);

            removeRunFiles(*run);
            RFMFactory::instance().removeRun(run->runNumber());
        }
    }
}

void FileDeleter::removeOldRuns()
{
    RFMFactory &factory(RFMFactory::instance());
    RFMFactory::RunNumberList old;
    uint32_t max = Config::instance().getDimHistoryCount();

    factory.getMigratedRuns(old);
    while (old.size() > max)
    {
        RFMFactory::RunNumberList::iterator it = old.begin();
        RunInfo::Shared run(factory.getRun(*it));

        LOG(INFO) << "[FileDeleter]: removing old run: " << *it;
        if (run)
        {
            for (MergedFile::Shared file : run->getFiles())
            {
                if (!(file->status() != MergedFile::MIGRATED))
                {
                    // must be ignored, let's try to finish/cleanup
                    Archiver::instance().finishFile(
                        Paths::getRemoteFilePath(*run, *file));
                }
                removeRawFiles(*run, *file);
            }
            Archiver::instance().finishRun(Paths::getRemotePath(*run));
            removeRunFiles(*run);
            run.reset();
        }
        if (factory.getRun(*it, false))
            LOG(WARNING) << "[FileDeleter]: old run still referenced: " << *it;

        factory.removeRun(*it);
        old.erase(it);
    }
}

void FileDeleter::removeRawFiles(const RunInfo &run, const MergedFile &file)
{ // File is staged
    if (Config::instance().isDeleterEnabled())
    {
        RFMFactory::DaqInfoList daqs(RFMFactory::instance().getDaqInfoList());
        for (FileEvent::Shared &event : file.getEvents())
        {
            for (DaqInfo::Shared &daq : daqs)
            {
                bfs::path rawFile = Paths::getRawFilePath(*daq, run, *event);
                LOG(INFO) << "[FileDeleter]: deleting local file: "
                          << rawFile.string();
                FileUtils::deleteFile(rawFile.string());
            }
        }
    }
    else
    {
        LOG(INFO)
            << "[FileDeleter]: file " << file.fileName()
            << " is staged but deleter is disabled, preserving local files";
    }

    std::lock_guard<std::mutex> lock(m_lock);
    m_files.erase(uid(file));
}

void FileDeleter::removeRunFiles(const RunInfo &run)
{
    if (Config::instance().isDeleterEnabled())
    {
        RFMFactory::DaqInfoList daqs(RFMFactory::instance().getDaqInfoList());
        for (DaqInfo::Shared &daq : daqs)
        {
            bfs::path dir = Paths::getPath(*daq, run);
            LOG(INFO) << "[FileDeleter]: deleting DAQ folder: " << dir.string();
            FileUtils::deleteDirectory(dir.string());
        }

        bfs::path runDir = Paths::getPath(run);
        LOG(INFO) << "[FileDeleter]: deleting run folder: " << runDir.string();
        FileUtils::deleteDirectory(runDir.string());
    }
    else
    {
        LOG(INFO) << "[FileDeleter]: run " << run.runNumber()
                  << " is archived (or expired) but deleter it disabled, "
                     "preserving run files";
    }

    std::lock_guard<std::mutex> lock(m_lock);
    m_runs.erase(run.runNumber());
}

std::string FileDeleter::uid(const MergedFile &file) const
{
    return std::to_string(file.runNumber()) + ":" +
        std::to_string(file.fileNumber());
}
