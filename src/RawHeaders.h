/*
 * RawHeaders.h
 *
 *  Created on: Jan 12, 2016
 *      Author: mdonze
 */

#ifndef RAWHEADERS_H_
#define RAWHEADERS_H_

#include <stdint.h>

typedef struct
{
    char title[4];      // Title
    uint32_t revNumber; // Revision number
    uint32_t reserved;  // Reserved flag
    uint32_t nbWords;   // Number of words following
} HEADER;

#endif /* RAWHEADERS_H_ */
