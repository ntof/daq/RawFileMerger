/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-01T17:39:13+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "RFMLockFile.hpp"

#include "Config.h"

#include <Singleton.hxx>

using ntof::utils::Flock;
using namespace ntof::rfm;

template class ntof::utils::Singleton<RFMLockFile>;

std::shared_ptr<Flock> RFMLockFile::getLockFile()
{
    std::shared_ptr<Flock> ret = m_lockFile.lock();

    if (!ret)
    {
        ntof::utils::SingletonMutex lock;
        if (m_lockFile.expired())
        { // still expired
            ret.reset(new Flock(Flock::Shared, Config::instance().lockFile()));
            ret->lock();
            m_lockFile = ret;
        }
        else
        {
            ret = m_lockFile.lock();
        }
    }
    return ret;
}
