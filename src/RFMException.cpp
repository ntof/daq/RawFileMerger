/*
 * RFMException.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: mdonze
 */

#include "RFMException.hpp"

#include <easylogging++.h>

namespace ntof {
namespace rfm {

RFMException::RFMException(const std::string &message) : msg(message)
{
    LOG(ERROR) << msg;
}

RFMException::RFMException(const char *message) : msg(message)
{
    LOG(ERROR) << msg;
}

RFMException::~RFMException() throw() {}

/**
 * Overload of std::exception
 * @return Error message
 */
const char *RFMException::what() const throw()
{
    return msg.c_str();
}

/**
 * Gets the error message
 * @return The error message
 */
const char *RFMException::getMessage() const throw()
{
    return msg.c_str();
}

} // namespace rfm
} /* namespace ntof */
