/*
 * RFMMonitoring.cpp
 *
 *  Created on: Jan 15, 2016
 *      Author: mdonze
 */

#include "RFMMonitoring.hpp"

#include <exception>
#include <set>
#include <sstream>

#include <DIMData.h>
#include <DIMUtils.hpp>
#include <dim_common.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Config.h"
#include "FileDeleter.hpp"
#include "Paths.hpp"
#include "RFMFactory.hpp"
#include "RFMUtils.hpp"

#define REFRESH_RATE 1000

using namespace ntof::rfm;
using namespace ntof::utils;
using namespace ntof::dim;

RFMMonitoring::RFMMonitoring() :
    Thread("RFMMonitoring"),
    m_info(Config::instance().getDimPrefix() + "/Info"),
    m_runs(Config::instance().getDimPrefix() + "/Runs"),
    m_current(Config::instance().getDimPrefix() + "/Current"),
    m_currentFiles(Config::instance().getDimPrefix() + "/Current/Files"),
    m_files(Config::instance().getDimPrefix() + "/Files"),
    m_currentHdlr(*this),
    m_state(Config::instance().getDimPrefix() + "/State"),
    m_maxErrorsWarns(Config::instance().getDimErrorsCount()),
    m_lastCode(0)
{
    setInterval(REFRESH_RATE);

    m_currentFiles.addData(FP_RUN_NUMBER, "runNumber", "", uint32_t(0),
                           AddMode::CREATE, false);
    m_currentFiles.addData(FP_FILES, "files", "", DIMData::List(),
                           AddMode::CREATE, false);
    m_currentFiles.addData(FP_INDEX, "index", "", DIMData::List(),
                           AddMode::CREATE, false);
    m_currentFiles.setHidden(FP_INDEX, true, true);

    for (const MergedFile::StatusNameMap::value_type &it :
         MergedFile::statusNames)
    {
        m_info.addData(it.first, it.second + "Count", "", uint64_t(0),
                       AddMode::CREATE, false);
    }

    m_info.addData(MergedFile::FAILED + 1, "transferred", "", uint64_t(0),
                   AddMode::CREATE, false);
    m_info.addData(MergedFile::FAILED + 2, "rate", "", uint64_t(0),
                   AddMode::CREATE, false);

    m_state.addStateValue(0, "OK");
    m_state.setValue(0);

    m_runs.setHandler(&m_runsHdlr);
    m_current.setHandler(&m_currentHdlr);
}

RFMMonitoring::~RFMMonitoring()
{
    //  stop thread before members start to destroy
    stop();
    m_runs.setHandler(nullptr);
    m_current.setHandler(nullptr);
}

void RFMMonitoring::setInterval(uint32_t ms)
{
    Thread::setInterval(std::chrono::milliseconds(ms));
}

void RFMMonitoring::setCurrentRun(const RunInfo &run)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_run = run.shared();
    m_updatedFiles.clear();

    for (MergedFile::Shared &file : run.getFiles())
        m_updatedFiles.insert(file->fileNumber());

    {
        const MergedFileIndex::Shared index(run.index());
        if (index)
            m_updatedFiles.insert(index->fileNumber());
    }
    m_updatedRuns.insert(run.runNumber());
}

RunInfo::Shared RFMMonitoring::currentRun() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_run;
}

void RFMMonitoring::notify(RunInfo::RunNumber run)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_updatedRuns.insert(run);
}

void RFMMonitoring::notify(RunInfo::RunNumber run,
                           MergedFile::FileNumber fileNumber)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_updatedRuns.insert(run);

    if (m_run && run == m_run->runNumber())
        m_updatedFiles.insert(fileNumber);
}

void RFMMonitoring::update()
{
    updateFiles();
    updateRuns();
    updateInfo();
}

void RFMMonitoring::thread_func()
{
    update();
}

void RFMMonitoring::updateFiles()
{
    RunInfo::Shared run;
    UpdatedFilesList updated;

    std::unique_lock<std::mutex> lock(m_lock);
    run = m_run;
    updated.swap(m_updatedFiles);
    lock.unlock();

    if (!run)
    {
        if (m_currentFiles.getValue<uint32_t>(FP_RUN_NUMBER) != 0)
        {
            m_currentFiles.setValue<uint32_t>(FP_RUN_NUMBER, 0);
            m_currentFiles.setValue<DIMData::List>(FP_FILES, DIMData::List());
            m_currentFiles.setHidden(FP_INDEX, true, true);
        }
        return;
    }

    RunInfo::FilesList files = run->getFiles();
    try
    {
        bool updateData = false;
        if (run->runNumber() != m_currentFiles.getValue<uint32_t>(FP_RUN_NUMBER))
        {
            m_currentFiles.setValue<uint32_t>(FP_RUN_NUMBER, run->runNumber());
            m_fileRates.clear();

            DIMData::List data;
            data.reserve(files.size());
            for (MergedFile::Shared &file : files)
                data.push_back(toLiveData(*file));

            m_currentFiles.setValue(FP_FILES, data, false);

            MergedFileIndex::Shared index = run->index();
            if (!index)
                m_currentFiles.setHidden(FP_INDEX, true, false);
            else if (updated.count(index->fileNumber()))
            {
                m_currentFiles.setValue(FP_INDEX, toLiveData(*index, true),
                                        false);
                m_currentFiles.setHidden(FP_INDEX, false, false);
            }
            updateData = true;
        }
        else
        {
            DIMData::List dataFiles = m_currentFiles.getNestedValue(FP_FILES);
            for (MergedFile::Shared &file : files)
            {
                const MergedFile::FileNumber fileNumber = file->fileNumber();
                DIMData::List::iterator dataIt = std::find_if(
                    dataFiles.begin(), dataFiles.end(),
                    [&fileNumber](DIMData &data) {
                        return data.getIndex() == fileNumber;
                    });

                if (dataIt == dataFiles.end())
                {
                    dataFiles.push_back(toLiveData(*file));
                    updateData = true;
                }
                else if (updated.count(fileNumber) != 0)
                {
                    *dataIt = toLiveData(*file);
                    updateData = true;
                }
            }
            MergedFileIndex::Shared index = run->index();
            if (index && updated.count(index->fileNumber()))
            {
                *m_currentFiles.lockDataAt(FP_INDEX) = toData(*index, true);
                updateData = true;
            }
            m_currentFiles.setValue(FP_FILES, std::move(dataFiles), false);
        }

        if (updateData)
            m_currentFiles.updateData();
    }
    catch (std::exception &ex)
    {
        LOG(ERROR) << "Failed to update runs service: " << ex.what();
    }
}

DIMData RFMMonitoring::toData(const MergedFile &file, bool index)
{
    DIMData::List values;
    MergedFile::Status status = file.status();
    values.emplace_back(FIP_STATUS, "status", "", MergedFile::toString(status));
    values.emplace_back(FIP_IGNORED, "ignored", "",
                        (status == MergedFile::IGNORED)); // FIXME

    std::size_t sz = file.size();
    if (sz == 0)
        sz = file.eventsSize(); /* reports 0 for indexes, which is fine */
    values.emplace_back(FIP_SIZE, "size", "", uint64_t(sz));
    values.back().setHidden(sz == 0);

    values.emplace_back(FIP_TRANSFERRED, "transferred", "",
                        uint64_t(file.transferred()));
    values.back().setHidden(file.transferred() == 0);

    if (index)
    {
        values.emplace_back(FIP_FILENAME, "fileName", "", file.fileName());
        return DIMData(FP_INDEX, "index", std::string(), values);
    }
    else
        return DIMData(file.fileNumber(), file.fileName(), std::string(),
                       values);
}

DIMData RFMMonitoring::toLiveData(const MergedFile &file, bool index)
{
    DIMData data = RFMMonitoring::toData(file, index);

    TransferRate &rate = m_fileRates[file.fileNumber()];
    rate.update(file.transferred());

    DIMData::List &list = data.getNestedValue();
    list.insert(list.begin() + FIP_RATE,
                DIMData(FIP_RATE, "rate", "", uint64_t(rate.rate)));
    list[FIP_RATE].setHidden(rate.rate == 0);
    return data;
}

void RFMMonitoring::updateRuns()
{
    bool currentUpdated = false;
    UpdatedRunsList updated;

    std::unique_lock<std::mutex> lock(m_lock);
    updated.swap(m_updatedRuns);
    RunInfo::RunNumber currentNumber = (m_run) ? m_run->runNumber() :
                                                 RunInfo::InvalidRunNumber;
    lock.unlock();

    for (RunInfo::RunNumber runNumber : updated)
    {
        RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
        if (!run)
        {
            try
            { // may have already been removed
                m_runs.removeParameter(runNumber, false);
            }
            catch (...)
            {}

            RunTransferRateMap::iterator it = m_runRates.find(runNumber);
            if (it != m_runRates.end())
            {
                /* not part of the rates any more */
                m_rate.transferred -= it->second.transferred;
                m_runRates.erase(it);
            }
            if (currentNumber == runNumber)
            {
                m_run.reset();
                m_current.clearParameters(true);
            }
        }
        else
        {
            run->updateStats();
            LockRef<DIMData> data = findRun(runNumber);
            DIMData::List &dataset = data->getNestedValue();

            dataset[RP_EXPERIMENT].setValue(run->experiment());
            dataset[RP_APPROVED].setValue(run->isApproved());
            dataset[RP_STARTDATE].setValue(uint32_t(run->startDate()));
            dataset[RP_STOPDATE].setValue(uint32_t(run->stopDate()));
            dataset[RP_STOPDATE].setHidden(!run->hasStopDate());

            dataset[RP_EXPIRYDATE].setValue(uint32_t(run->expiryDate()));
            dataset[RP_EXPIRYDATE].setHidden(!run->hasExpiryDate());

            dataset[RP_TRANSFERRED].setValue(uint64_t(run->transferredBytes()));
            dataset[RP_TRANSFERRED].setHidden(run->transferredBytes() == 0);

            TransferRate &rate = m_runRates[runNumber];
            rate.update(run->transferredBytes());
            dataset[RP_TRANSFERRATE].setValue(uint64_t(rate.rate));
            dataset[RP_TRANSFERRATE].setHidden(uint64_t(rate.rate) == 0);

            FileStats stats = run->fileStats();
            for (const MergedFile::StatusNameMap::value_type &it :
                 MergedFile::statusNames)
            {
                dataset[RP_STATS + it.first].setValue(uint64_t(stats[it.first]));
                dataset[RP_STATS + it.first].setHidden(stats[it.first] <= 0);
            }

            if (rate.rate >= 1 || stats.at(MergedFile::TRANSFERRING) > 0)
            {
                // trigger in next loop to update rate
                notify(runNumber);
            }

            if (currentNumber == runNumber)
            {
                /*
                note: mustn't try to take the dim lock whilst modifying a
                service, `dataset` is still being modified (locked), this can
                create a dim deadlock if a command is received on `dataset`
                service meanwhile
                 */
                updateCurrentRun(dataset);
                currentUpdated = true;
            }
        }
    }
    if (!updated.empty())
        m_runs.updateList();
    if (currentUpdated)
        m_current.updateList();
}

LockRef<DIMData> RFMMonitoring::findRun(RunInfo::RunNumber runNumber)
{
    try
    {
        return m_runs.lockParameterAt(runNumber);
    }
    catch (DIMException &ex)
    {
        DIMData::List dataset;
        dataset.emplace_back(RP_RUN_NUMBER, "runNumber", "",
                             uint32_t(runNumber));
        dataset.emplace_back(RP_EXPERIMENT, "experiment", "", std::string());
        dataset.emplace_back(RP_APPROVED, "approved", "", false);
        dataset.emplace_back(RP_STARTDATE, "startDate", "", uint32_t(0));
        dataset.emplace_back(RP_STOPDATE, "stopDate", "", uint32_t(0));

        dataset.emplace_back(RP_EXPIRYDATE, "expiryDate", "", uint32_t(0));
        dataset.emplace_back(RP_TRANSFERRED, "transferred", "", uint64_t(0));
        dataset.emplace_back(RP_TRANSFERRATE, "rate", "", uint64_t(0));

        for (const MergedFile::StatusNameMap::value_type &it :
             MergedFile::statusNames)
        {
            dataset.emplace_back(RP_STATS + it.first, it.second + "Count", "",
                                 uint64_t(0));
        }
        m_runs.addParameter(runNumber, "run", "", std::move(dataset),
                            AddMode::CREATE, false);
    }
    return m_runs.lockParameterAt(runNumber);
}

void RFMMonitoring::updateCurrentRun(const DIMData::List &dataset)
{
    /* +1 and <=1 stands for the builtin parameters count param */
    if (m_current.getParameterCount() <= 1)
    {
        /* we don't care about type/value, it will be overriden right away */
        for (const DIMData &d : dataset)
            m_current.addParameter(d.getIndex() + 1, d.getName(), d.getUnit(),
                                   int64_t(0), AddMode::CREATE, false);
    }

    for (const DIMData &d : dataset)
    {
        LockRef<DIMData> currentData = m_current.lockParameterAt(d.getIndex() +
                                                                 1);
        currentData->setValue(d);
        currentData->setHidden(d.isHidden());
    }
}

void RFMMonitoring::updateInfo()
{
    bool updated = false;
    std::size_t transferred = 0;
    for (const RunTransferRateMap::value_type &it : m_runRates)
    {
        transferred += it.second.transferred;
    }
    m_rate.update(transferred);
    VLOG(1) << "[stats] total rate:" << m_rate.rate
            << " transferred:" << transferred;

    FileStats stats;
    RFMFactory::instance().getFileStats(stats);

    for (const MergedFile::StatusNameMap::value_type &it :
         MergedFile::statusNames)
    {
        if (m_info.getValue<uint64_t>(it.first) != stats[it.first])
        {
            updated = true;
            m_info.setValue(it.first, uint64_t(stats[it.first]), false);
        }
    }

    if (m_info.getValue<uint64_t>(MergedFile::FAILED + 1) !=
        uint64_t(transferred))
    {
        updated = true;
        m_info.setValue(MergedFile::FAILED + 1, uint64_t(transferred), false);
    }

    if (m_info.getValue<uint64_t>(MergedFile::FAILED + 2) !=
        uint64_t(m_rate.rate))
    {
        updated = true;
        m_info.setValue(MergedFile::FAILED + 2, uint64_t(m_rate.rate), false);
    }
    if (updated)
        m_info.updateData();
}

void RFMMonitoring::setWarning(const std::string &ident, const std::string &msg)
{
    ntof::dim::DIMLockGuard lock;
    ErrorMap::iterator it = m_warnings.find(ident);
    if (it != m_warnings.end())
    {
        if (msg.empty())
        {
            // remove
            m_state.removeWarning(it->second);
            m_codes.erase(it->second);
            m_warnings.erase(it);
        }
        else
        {
            m_state.addWarning(it->second, msg);
        }
    }
    else if (!msg.empty())
    {
        int32_t code = 1;
        if (!getNextCode(m_warnings, code))
        {
            LOG(WARNING) << "Dropping warning message: " << msg;
            return;
        }
        m_warnings[ident] = code;
        m_codes.insert(code);
        m_state.addWarning(code, msg);
    }
}

void RFMMonitoring::setError(const std::string &ident, const std::string &msg)
{
    ntof::dim::DIMLockGuard lock;
    ErrorMap::iterator it = m_errors.find(ident);
    if (it != m_errors.end())
    {
        if (msg.empty())
        {
            // remove
            m_state.removeError(it->second);
            m_codes.erase(it->second);
            m_errors.erase(it);
        }
        else
        {
            m_state.addError(it->second, msg);
        }
    }
    else if (!msg.empty())
    {
        int32_t code = 1;
        if (!getNextCode(m_errors, code))
        {
            LOG(WARNING) << "Dropping error message: " << msg;
            return;
        }
        m_errors[ident] = code;
        m_codes.insert(code);
        m_state.addError(code, msg);
    }
}

bool RFMMonitoring::getNextCode(ErrorMap &map, int32_t &code)
{
    if (map.size() >= m_maxErrorsWarns)
        return false;

    do
    {
        if (++m_lastCode < 0)
            m_lastCode = 0;
    } while (m_codes.count(m_lastCode) != 0);

    code = m_lastCode;
    return true;
}

void RFMMonitoring::errorHandler(int severity, int code, char *msg)
{
    if (DIMDNSDUPLC == code)
    {
        LOG(ERROR) << "Fatal : " << msg;
        exit(1);
    }
    if (severity != DIM_INFO)
    {
        LOG(ERROR) << msg;
    }
}
