/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-09T14:53:05+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef MERGEDFILEINDEX_HPP__
#define MERGEDFILEINDEX_HPP__

#include <memory>

#include "MergedFile.hpp"
#include "Types.hpp"

namespace ntof {
namespace rfm {

class RFMDatabase;
class RFMFactory;

class MergedFileIndex : public MergedFile
{
public:
    typedef std::shared_ptr<MergedFileIndex> Shared;

    std::string fileName() const override;

    inline Shared shared() const
    {
        return std::static_pointer_cast<MergedFileIndex>(MergedFile::shared());
    }

protected:
    friend class RFMDatabase;
    friend class RFMFactory;

    explicit MergedFileIndex(RunNumber runNumber);
};

} // namespace rfm
} // namespace ntof

#endif
