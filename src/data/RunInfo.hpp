/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T12:48:57+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef RUNINFO_HPP__
#define RUNINFO_HPP__

#include <ctime>
#include <memory>
#include <mutex>
#include <set>
#include <vector>

#include <Worker.hpp>

#include "DaqInfo.hpp"
#include "FileStats.hpp"
#include "MergedFile.hpp"
#include "MergedFileIndex.hpp"
#include "Storable.hpp"
#include "Types.hpp"

namespace ntof {
namespace rfm {

class RFMDatabase;
class RFMFactory;

/**
 * @brief Run information object
 * @details use RFMFactory to retrieve/manipulate this object
 * @details all public methods are thread safe
 */
class RunInfo : public Storable<RunInfo>
{
public:
    typedef std::shared_ptr<RunInfo> Shared;
    typedef ntof::rfm::RunNumber RunNumber;
    typedef std::vector<MergedFile::Shared> FilesList;
    typedef std::set<DaqInfo::CrateId> DaqsList;
    typedef int32_t Flags;

    static const RunNumber InvalidRunNumber;
    static const std::time_t NoExpiry;
    static const std::time_t Expired;

    enum Flag
    {
        Approved = 0x01,
        Standalone = 0x02
    };

    RunInfo(const RunInfo &) = delete;
    RunInfo &operator=(const RunInfo &) = delete;
    ~RunInfo();

    /**
     * @brief retrieve runNumber
     */
    inline RunInfo::RunNumber runNumber() const { return m_runNumber; }

    /**
     * @brief retrieve experimental area (EAR1/EAR2/LAB)
     */
    inline const std::string &expArea() const { return m_expArea; }

    /**
     * @brief retrieve run start date
     * @details should always be valid
     */
    inline std::time_t startDate() const { return m_startDate; }

    /**
     * @brief retrieve the year relative to startDate
     * @return complete year (tm_year + 1900)
     */
    int year() const;

    /**
     * @brief retrieve run stop date
     * @details may be invalid (-1)
     */
    std::time_t stopDate() const;

    /**
     * @brief set the run stop date
     * @return false if the run was already stopped
     */
    bool setStopDate(std::time_t stop);

    /**
     * @brief check if stopDate is valid
     * @return return true if stopDate is set
     */
    inline bool hasStopDate() const { return stopDate() != std::time_t(-1); }

    /**
     * @brief retrieve the run expiry date
     */
    std::time_t expiryDate() const;

    /**
     * @brief set expiry date
     */
    bool setExpiryDate(std::time_t date);

    /**
     * @brief check if stopDate is valid
     * @return return true if stopDate is set
     */
    inline bool hasExpiryDate() const
    {
        return expiryDate() != RunInfo::NoExpiry;
    }

    /**
     * @brief retrieve Daqs list
     * @return returns a copy of the list (to be thread safe)
     */
    DaqsList getDaqs() const;

    /**
     * @brief test if the following daq is used by this run
     * @param[in]  DaqInfo::CrateId the crate to check
     */
    bool hasDaq(DaqInfo::CrateId id) const;

    /**
     * @brief retrieve run experiment name
     */
    std::string experiment() const;

    /**
     * @brief set experiment name
     */
    bool setExperiment(const std::string &exp);

    /**
     * @brief retrieve current file stats
     * @return FileStats
     */
    FileStats fileStats() const;

    /**
     * @brief get transferred bytes count
     */
    std::size_t transferredBytes() const;

    /**
     * @brief update transferred bytes count
     * @param[in] size bytes count to set
     */
    void setTransferredBytes(std::size_t size);

    /**
     * @brief update FileStats and transferred
     */
    void updateStats();

    /**
     * @brief add a new merged file associated with this run
     */
    MergedFile::Shared addFile();

    /**
     * @brief get current file
     */
    MergedFile::Shared currentFile() const;

    /**
     * @brief retrieves all files associated with this run
     */
    FilesList getFiles() const;

    /**
     * @brief check if run has any raw files
     */
    bool hasFiles() const;

    /**
     * @brief find file with the given fileNumber
     */
    MergedFile::Shared findFile(MergedFile::FileNumber number) const;

    /**
     * @brief retrieve current index
     */
    inline MergedFileIndex::Shared index() const { return m_index; };

    /**
     * @brief create an index file
     * @details adding subsequent files to the run will fail
     */
    MergedFileIndex::Shared createIndex();

    /**
     * @brief check if run is a standaloneDaq run
     */
    bool isStandalone() const;
    void setStandalone(bool value = true);

    /**
     * @brief check if a run has been approved
     */
    bool isApproved() const;

    /**
     * @brief flag a run as approved
     */
    bool setApproved(bool value);

protected:
    friend class RFMDatabase;
    friend class RFMFactory;

    explicit RunInfo(RunNumber runNumber,
                     const std::string &expArea,
                     std::time_t startDate);

    MergedFile::FileNumber getNextFileNumber() const;
    void addStats(const MergedFile &file);

    const RunNumber m_runNumber;
    const std::string m_expArea;
    const std::time_t m_startDate;
    std::time_t m_stopDate;
    std::time_t m_expiryDate;
    DaqsList m_daqs;
    std::string m_experiment;
    Flags m_flags;

    FilesList m_files;
    MergedFileIndex::Shared m_index;

    FileStats m_stats;
    std::size_t m_transferred;
};

} // namespace rfm
} // namespace ntof

#endif
