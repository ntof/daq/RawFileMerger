/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T13:44:54+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "DaqInfo.hpp"

#include <cstdlib>
#include <sstream>
#include <string>

#include <easylogging++.h>

#include "Config.h"
#include "FileEvent.hpp"
#include "RunInfo.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

std::string getName(const std::string &hostname)
{
    std::string::size_type dotPos = hostname.find_first_of('.');
    return (dotPos != std::string::npos) ? hostname.substr(0, dotPos) :
                                           hostname;
}

DaqInfo::DaqInfo(CrateId crateId, const std::string &hostname) :
    m_crateId(crateId), m_hostname(hostname), m_name(getName(hostname))
{}

bool DaqInfo::toString(const DaqInfo::CrateIdList &daqs, std::string &out)
{
    std::ostringstream oss;

    bool first = true;
    for (CrateId id : daqs)
    {
        if (!first)
            oss << ":" << id;
        else
            oss << id;
        first = false;
    }
    out = oss.str();
    return true;
}

bool DaqInfo::fromString(const std::string &daqs, DaqInfo::CrateIdList &out)
{
    CrateId id;
    const char *pos;
    char *end;

    if (daqs.empty())
        return true;

    pos = daqs.c_str();
    while (pos != nullptr)
    {
        id = std::strtoul(pos, &end, 10);
        if (end == nullptr || *end == '\0')
            pos = nullptr;
        else if ((*end != ':') || (end == pos))
            break;
        else
            pos = end + 1;
        out.insert(id);
    }
    return pos == nullptr;
}
