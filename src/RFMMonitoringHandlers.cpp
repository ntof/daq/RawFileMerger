/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-25T18:19:33+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "RFMMonitoringHandlers.hpp"

#include <ctime>

#include <easylogging++.h>

#include "RFMFactory.hpp"
#include "RFMMonitoring.hpp"
#include "data/RunInfo.hpp"

using namespace ntof::rfm;
using namespace ntof::dim;

int RunsParamListHandler::parameterChanged(std::vector<DIMData> &settingsChanged,
                                           const DIMParamList & /*list*/,
                                           int &errCode,
                                           std::string &errMsg)
{
    try
    {
        for (const DIMData &settings : settingsChanged)
        {
            const DIMData::List &params = settings.getNestedValue();
            RunInfo::RunNumber runNumber = RunInfo::InvalidRunNumber;
            std::string experiment;
            bool isExperiment = false;
            int approved = -1;
            std::time_t expiry = std::time_t(-1);

            for (const DIMData &data : params)
            {
                switch (data.getIndex())
                {
                case RFMMonitoring::RP_RUN_NUMBER:
                    runNumber = data.getUIntValue();
                    break;
                case RFMMonitoring::RP_EXPERIMENT:
                    experiment = data.getStringValue();
                    isExperiment = true;
                    break;
                case RFMMonitoring::RP_APPROVED:
                    approved = data.getBoolValue() ? 1 : 0;
                    break;
                case RFMMonitoring::RP_EXPIRYDATE:
                    expiry = data.getUIntValue();
                    break;
                default:
                    throw DIMException("Non modifiable value: " +
                                           std::to_string(data.getIndex()),
                                       __LINE__, EINVAL);
                    break;
                }
            }
            if (runNumber == RunInfo::InvalidRunNumber)
                throw DIMException("missing runNumber information", __LINE__,
                                   EINVAL);

            RunInfo::Shared run(RFMFactory::instance().getRun(runNumber));
            if (!run)
                throw DIMException("run not found: " + std::to_string(runNumber),
                                   __LINE__, EINVAL);

            if (isExperiment && !run->setExperiment(experiment))
                throw DIMException("failed to set experiment", __LINE__, EINVAL);

            if (expiry != std::time_t(-1) && !run->setExpiryDate(expiry))
                throw DIMException("failed set expiry", __LINE__, EINVAL);

            if (approved != -1 && !run->setApproved(approved))
                throw DIMException("failed to approve run", __LINE__, EINVAL);

            run->save();
        }
    }
    catch (DIMException &ex)
    {
        LOG(ERROR)
            << "[RunsParamListHandler] request failed: " << ex.getMessage();
        errCode = ex.getErrorCode();
        errMsg = ex.getMessage();
        return -1;
    }
    settingsChanged.clear(); // changes will come from the DB
    return 0;
}

CurrentRunParamListHandler::CurrentRunParamListHandler(RFMMonitoring &mon) :
    m_mon(mon)
{}

int CurrentRunParamListHandler::parameterChanged(
    std::vector<DIMData> &settingsChanged,
    const DIMParamList & /*list*/,
    int &errCode,
    std::string &errMsg)
{
    try
    {
        RunInfo::Shared run = m_mon.currentRun();

        if (!run)
            throw DIMException("no current run", __LINE__, ENOENT);

        std::string experiment;
        bool isExperiment = false;
        int approved = -1;
        std::time_t expiry = std::time_t(-1);

        for (const DIMData &data : settingsChanged)
        {
            switch (data.getIndex())
            {
            case RFMMonitoring::CRP_RUN_NUMBER:
                if (data.getUIntValue() != run->runNumber())
                    throw DIMException("invalid runNumber", __LINE__, EINVAL);
                break;
            case RFMMonitoring::CRP_EXPERIMENT:
                experiment = data.getStringValue();
                isExperiment = true;
                break;
            case RFMMonitoring::CRP_APPROVED:
                approved = data.getBoolValue() ? 1 : 0;
                break;
            case RFMMonitoring::CRP_EXPIRYDATE:
                expiry = data.getUIntValue();
                break;
            default:
                throw DIMException(
                    "Non modifiable value: " + std::to_string(data.getIndex()),
                    __LINE__, EINVAL);
                break;
            }
        }

        if (isExperiment && !run->setExperiment(experiment))
            throw DIMException("failed to set experiment", __LINE__, EINVAL);

        if (expiry != std::time_t(-1) && !run->setExpiryDate(expiry))
            throw DIMException("failed set expiry", __LINE__, EINVAL);

        if (approved != -1 && !run->setApproved(approved))
            throw DIMException("failed to approve run", __LINE__, EINVAL);

        run->save();
    }
    catch (DIMException &ex)
    {
        errCode = ex.getErrorCode();
        errMsg = ex.getMessage();
        return -1;
    }
    settingsChanged.clear(); // changes will come from the DB
    return 0;
}

FilesRpc::FilesRpc(const std::string &service) : DIMXMLRpc(service) {}

void FilesRpc::rpcReceived(ntof::dim::DIMCmd &cmd)
{
    LOG(TRACE) << "[RFMMonitoring]: rpc request";
    try
    {
        pugi::xml_node node = cmd.getData();
        const std::string name = node.attribute("name").as_string("");
        const RunInfo::RunNumber runNumber =
            node.attribute("runNumber").as_uint(RunInfo::InvalidRunNumber);

        if (name != std::string("listFiles"))
            throw DIMException("Invalid command name: " + name, __LINE__,
                               EINVAL);

        if (runNumber == RunInfo::InvalidRunNumber)
            throw DIMException("Invalid command runNumber", __LINE__, EINVAL);

        RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
        if (!run)
            throw DIMException("No such run: " + std::to_string(runNumber),
                               __LINE__, ENOENT);

        DIMAck ack = makeReply(cmd.getKey());
        DIMData data = DIMData(RFMMonitoring::FP_RUN_NUMBER, "runNumber",
                               std::string(), uint32_t(runNumber));
        data.insertInto(ack.getRoot());
        {
            RunInfo::FilesList files = run->getFiles();
            DIMData::List list;
            list.reserve(files.size());

            for (MergedFile::Shared &file : files)
                list.push_back(RFMMonitoring::toData(*file));
            data = DIMData(RFMMonitoring::FP_FILES, "files", std::string(),
                           std::move(list));
            data.insertInto(ack.getRoot());
        }
        {
            MergedFileIndex::Shared index = run->index();
            if (index)
            {
                data = RFMMonitoring::toData(*index, true);
                data.insertInto(ack.getRoot());
            }
        }
        setReply(ack);
    }
    catch (DIMException &ex)
    {
        setError(cmd.getKey(), ex.getErrorCode(), ex.getMessage());
    }
}
