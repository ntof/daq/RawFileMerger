/*
 * RFMException.hpp
 *
 *  Created on: Jan 8, 2016
 *      Author: mdonze
 */

#ifndef RFMEXCEPTION_H_
#define RFMEXCEPTION_H_

#include <exception>
#include <string>

namespace ntof {
namespace rfm {

class RFMException : public std::exception
{
public:
    explicit RFMException(const std::string &message);
    explicit RFMException(const char *message);
    virtual ~RFMException() throw();

    /**
     * Overload of std::exception
     * @return Error message
     */
    virtual const char *what() const throw();

    /**
     * Gets the error message
     * @return The error message
     */
    const char *getMessage() const throw();

private:
    std::string msg; //!< Error message
};

} // namespace rfm
} /* namespace ntof */

#endif /* RFMEXCEPTION_H_ */
