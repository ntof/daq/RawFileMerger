//
// Created by matteof on 01/07/2020.
//

#ifndef CONFIG_H
#define CONFIG_H

#include <memory>

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <pugixml.hpp>
#include <sys/types.h>

namespace ntof {
namespace rfm {

class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    enum class ReplaceStrategy
    {
        RENAME,
        DELETE,
        IGNORE,
        UNKNOWN = 255
    }; //!< CASTOR file replacement strategy
    static std::string toString(ReplaceStrategy strategy);
    static bool fromString(const std::string &str, ReplaceStrategy &out);

    enum class ListeningMode
    {
        NONE,
        DAQ,
        EACS
    }; //!< RFMerger listening mode
    static std::string toString(ListeningMode mode);
    static bool fromString(const std::string &str, ListeningMode &out);

    struct Paths
    {
        std::string local;    //!< local eacs path
        std::string remote;   //!< remote tape storage path
        std::string mount;    //!< daq datab path
        std::string database; //!< sqlite database path
    };

    struct PreAnalysis
    {
        std::string cmd;       //!< command to run
        std::size_t skipCount; //!< files to skip
        bool checkErrorCode;   //!< retry on error
        bool onlyNew;          //!< Accept only newer events
    };

    enum class RemoteSubdirs
    {
        FULL,
        RUN,
        NONE
    }; //<! remote subdirs pattern
    static std::string toString(RemoteSubdirs mode);
    static bool fromString(const std::string &str, RemoteSubdirs &out);

    /*
     * \brief Destructor of the class
     */
    virtual ~Config() = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     *
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file = configFile,
                        const std::string &miscFile = configMiscFile);

    const Paths &getPaths() const;
    const std::string &getCastorNode() const;
    inline const std::string &getCastorOptions() const
    {
        return castorOptions_;
    }
    const std::string &getSvcClass() const;
    const std::string &getDimPrefix() const;
    uint32_t getDimHistoryCount() const;
    uint32_t getDimErrorsCount() const;
    uint32_t getMaxEvents() const;
    uint64_t getMinTotalSize() const;
    uint32_t getDefaultExpiry() const { return m_expiry; };
    ReplaceStrategy getReplaceStrategy() const;
    uint32_t getPoolSize() const;
    uint64_t getBufferSize() const;
    const std::string &getArchiverName() const;

    /**
     * @brief get listening mode
     * @details
     * - EACS: listen for EACS/Validation events
     * - DAQ: listen for ntofdaq-mX/WRITER/RunExtensionNumber events
     * - NONE: regular mode, wait for EACS commands
     */
    inline ListeningMode getListeningMode() const { return listen_; }
    inline void setListeningMode(ListeningMode mode) { listen_ = mode; }

    bool isDeleterEnabled() const;
    void setDeleterEnabled(bool value);

    /**
     * @brief PreAnalysis configuration
     * @return nullptr when archiver != PreAnalysisArchiver
     */
    inline const PreAnalysis *getPreAnalysisConfig() const
    {
        return m_preAnalysis.get();
    }

    /**
     * @brief whereas subdirs should be created on remote
     */
    inline RemoteSubdirs getRemoteSubdirs() const { return m_remoteSubdirs; }
    inline void setRemoteSubdirs(RemoteSubdirs value)
    {
        m_remoteSubdirs = value;
    }

    static const std::string configFile;     //!< Path of the config file
    static const std::string configMiscFile; //!< Path of the global config file
    static const std::string defaultDatabaseName; //!< Default path to the
                                                  //!< database

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &miscFile = configMiscFile);

    Paths paths_;            //!< CASTOR paths
    std::string castorNode_; //!< CASTOR cluster node
    std::string castorOptions_;
    std::string svcClass_;       //!< CASTOR svc class
    std::string dimPrefix_;      //!< DIM service prefix
    uint32_t dimHistoryCount_;   //!< DIM service history count
    uint32_t dimErrorsCount_;    //!< DIM service errors count
    uint64_t minTotalSize_;      //!< Minimum total size of resulting copy
    uint32_t maxEvents_;         //!< Maximum events before triggering copy
    uint32_t m_expiry;           //!< Default expiry delay
    ReplaceStrategy replaceSty_; //!< What to do if file exists on CASTOR
    uint32_t poolSize_;          //!< Thread pool size
    uint64_t bufferSize_;        //!< Buffer size
    ListeningMode listen_;       //!< listening mode
    bool deleterEnabled_;        //!< test if deleter is enabled
    std::string archiverName_;   //!< name of the archiver to use
    std::unique_ptr<PreAnalysis> m_preAnalysis;
    RemoteSubdirs m_remoteSubdirs;

    /**
     * Helper function to extract the path attribute from a node
     * @param name Name of the wanted path
     * @param pathNode
     * @return
     */
    std::string getPath(const std::string &name,
                        const pugi::xml_node &pathNode) const;

    /**
     * Parse a string giving a size
     * @param sizeStr
     * @return
     */
    uint64_t parseSize(const std::string &sizeStr) const;

    ReplaceStrategy getReplaceStrategyFromString(std::string &sty) const;
};

} // namespace rfm
} // namespace ntof

#endif // CONFIG_H
