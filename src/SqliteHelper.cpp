/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-02T15:47:13+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "SqliteHelper.hpp"

#include <ctime>

namespace sqlite {

StmtGuard::StmtGuard(sqlite3_stmt *stmt) : autoIndex(0), stmt(stmt) {}

StmtGuard::~StmtGuard()
{
    if (stmt)
    {
        sqlite3_reset(stmt);
    }
}

int StmtGuard::step()
{
    autoIndex = 0;
    return sqlite3_step(stmt);
}

int StmtGuard::bind(int64_t value, int idx)
{
    return sqlite3_bind_int64(stmt, (idx < 0) ? ++autoIndex : idx, value);
}

int StmtGuard::bind(const std::string &value, int idx)
{
    return sqlite3_bind_text(stmt, (idx < 0) ? ++autoIndex : idx, value.c_str(),
                             value.length(), nullptr);
}

template<>
int32_t StmtGuard::get<>(int idx)
{
    return sqlite3_column_int(stmt, (idx < 0) ? autoIndex++ : idx);
}

template<>
int64_t StmtGuard::get<>(int idx)
{
    return sqlite3_column_int64(stmt, (idx < 0) ? autoIndex++ : idx);
}

template<>
bool StmtGuard::get<>(int idx)
{
    return sqlite3_column_int(stmt, (idx < 0) ? autoIndex++ : idx) != 0;
}

template<>
std::string StmtGuard::get<>(int idx)
{
    const char *str = reinterpret_cast<const char *>(
        sqlite3_column_text(stmt, (idx < 0) ? autoIndex++ : idx));
    return (str == nullptr) ? std::string() : std::string(str);
}

} // namespace sqlite
