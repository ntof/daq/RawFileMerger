/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T12:50:47+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef ARCHIVER_HPP__
#define ARCHIVER_HPP__

#include <cstdint>
#include <memory>
#include <string>

#include <boost/filesystem.hpp>

#include <Singleton.hpp>

#include "RFMException.hpp"

namespace ntof {
namespace rfm {

class Archiver : public ntof::utils::Singleton<Archiver>
{
public:
    virtual ~Archiver() = default;

    class Exception : public RFMException
    {
    public:
        explicit Exception(const std::string &what) : RFMException(what) {}
    };

    class File
    {
    public:
        typedef std::shared_ptr<File> Shared;

        virtual ~File() = default;

        /**
         * @brief write file at the given offset
         * @throws Archiver::Exception on error
         */
        virtual void write(uint64_t offset,
                           uint32_t size,
                           const void *buffer) = 0;

        /**
         * @brief finalize the file
         * @throws Archiver::Exception on error
         */
        virtual void close() = 0;
    };

    struct FileInfo
    {
        FileInfo() : exists(false), ignore(false), fileSize(0) {}
        FileInfo(bool exists, uint64_t size) :
            exists(exists), ignore(false), fileSize(size)
        {}

        bool exists;
        bool ignore;
        uint64_t fileSize;

        inline operator bool() const { return exists; }
    };

    /**
     * @brief get the archiver name
     */
    virtual const std::string &name() const = 0;

    /**
     * @open a file to migrate to this archiver
     * @throws Archiver::Exception on error
     */
    virtual File::Shared open(const boost::filesystem::path &file) = 0;

    /**
     * @brief test if file has been archived
     * @throws Archiver::Exception on error
     */
    virtual bool isMigrated(const boost::filesystem::path &file) = 0;

    /**
     * @brief get basic information about the file
     * @throws Archiver::Exception on error
     */
    virtual FileInfo getInfo(const boost::filesystem::path &file) = 0;

    /**
     * @brief create a remote directory on the archiver
     * @throws Archiver::Exception on error
     */
    virtual void createDir(const boost::filesystem::path &file) = 0;

    /**
     * @brief delete a remote file
     * @throws Archiver::Exception on error
     */
    virtual void deleteFile(const boost::filesystem::path &file) = 0;

    /**
     * @brief rename a remote file
     * @throws Archiver::Exception on error
     */
    virtual void renameFile(const boost::filesystem::path &file,
                            const boost::filesystem::path &newFile) = 0;

    /**
     * @brief terminate file processing
     * @details called once Deleter has checked and validated the file
     */
    virtual void finishFile(const boost::filesystem::path &file);

    /**
     * @brief terminate run processing
     * @details called once Deleter has checked and validated the file
     */
    virtual void finishRun(const boost::filesystem::path &run);

protected:
    friend class ntof::utils::Singleton<Archiver>;
};

} // namespace rfm

namespace utils {
template<>
ntof::rfm::Archiver &Singleton<ntof::rfm::Archiver>::instance();
}

} // namespace ntof

#endif
