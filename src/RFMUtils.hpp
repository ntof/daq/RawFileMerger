/*
 * RFMUtils.hpp
 *
 *  Created on: Nov 3, 2015
 *      Author: mdonze
 */

#ifndef RFMUTILS_HPP__
#define RFMUTILS_HPP__

#include <chrono>
#include <cstdint>
#include <streambuf>
#include <string>
#include <vector>

// see: https://bugzilla.redhat.com/show_bug.cgi?id=999320
#include <boost/config.hpp>
#if defined(__clang__) && __cplusplus >= 201103L
#undef BOOST_NO_CXX11_HDR_TUPLE
#endif
#include <boost/signals2.hpp>

#include <pthread.h>

#include "data/MergedFile.hpp"

namespace ntof {
namespace rfm {

class TransferRate
{
public:
    TransferRate();
    void update(std::size_t transferred);

    std::size_t transferred;
    double rate;

protected:
    std::chrono::time_point<std::chrono::steady_clock> m_last;
};

typedef boost::signals2::signal<void(const std::string &id,
                                     const std::string &message)>
    ErrorSignal;
typedef ErrorSignal WarningSignal;

typedef boost::signals2::signal<void(MergedFile &file,
                                     const std::string &errorMsg)>
    FileErrorSignal;

typedef boost::signals2::signal<void(const std::string &err)> TaskSignal;

class RWLock
{
public:
    RWLock();
    ~RWLock();

    bool lock(bool write = false);
    void unlock();

protected:
    pthread_rwlock_t m_lock;
};

/**
 * @brief streambuf implementation for vector<char>
 */
class vstreambuf : public std::streambuf
{
public:
    explicit vstreambuf(std::vector<char> &vec);

    std::streampos seekoff(std::streamoff off,
                           std::ios_base::seekdir way,
                           std::ios_base::openmode which = std::ios_base::in |
                               std::ios_base::out);
    std::streampos seekpos(std::streampos pos,
                           std::ios_base::openmode which = std::ios_base::in |
                               std::ios_base::out);
};

template<typename T>
std::string dumpContainer(const T &t)
{
    std::ostringstream oss;
    oss << "[";
    bool first = true;
    for (const typename T::value_type &value : t)
    {
        if (first)
            oss << value;
        else
            oss << ", " << value;
        first = false;
    }
    oss << "]";
    return oss.str();
}

bool endsWith(const std::string &str, const std::string &needle);

} // namespace rfm
} /* namespace ntof */

#endif
