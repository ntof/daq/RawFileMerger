/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T09:18:05+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "RFMFactory.hpp"

#include <thread>

#include <easylogging++.h>

#include "Config.h"
#include "RFMException.hpp"

#include "data/Storable.hxx"
#include <Singleton.hxx>

using namespace ntof::rfm;
using namespace ntof::utils;

template class ntof::utils::Singleton<RFMFactory>;

const std::chrono::milliseconds RFMFactory::WorkQueueFullDelay(100);

RFMFactory::RFMFactory() :
    m_worker("Database", 1, 2048), m_db(Config::instance().getPaths().database)
{
    for (const Config::DaqMap::value_type &daq : Config::instance().getDaq())
    {
        m_daqs[daq.first].reset(new DaqInfo(daq.first, daq.second));
    }
}

RFMFactory::~RFMFactory()
{
    /* stop the worker before the db closes */
    m_worker.stop();
}

RunInfo::Shared RFMFactory::createRun(RunInfo::RunNumber runNumber,
                                      const std::string &expArea,
                                      std::time_t startDate,
                                      const std::set<DaqInfo::CrateId> &daqs,
                                      const std::string &experiment)
{
    RunInfo::Shared run;
    std::unique_lock<std::mutex> lock(m_lock);

    Cache::mapped_type &item = m_cache[runNumber];
    item.ref.reset();

    Worker::SharedTask task = post(
        [this, runNumber, expArea, startDate, daqs, experiment, &run] {
            run = this->m_db.insertRun(runNumber, expArea, startDate, daqs,
                                       experiment);
            std::lock_guard<std::mutex> lock(this->m_lock);
            if (run)
            {
                Cache::mapped_type &item = this->m_cache[runNumber];
                item.ref = run;
                item.loader.reset();
            }
            else
            {
                this->m_cache.erase(runNumber);
                throw this->m_db.errorString();
            }
        });
    item.loader = task;
    lock.unlock();

    if (task)
        task->wait();
    if (run)
        runAddSignal(run);
    return run;
}

RunInfo::Shared RFMFactory::getRun(RunInfo::RunNumber runNumber, bool load)
{
    RunInfo::Shared run;
    Worker::SharedTask loader;
    std::unique_lock<std::mutex> lock(m_lock);

    Cache::const_iterator it = m_cache.find(runNumber);
    if (it != m_cache.end())
    {
        run = it->second.ref.lock();
        if (run)
            return run;
        loader = it->second.loader;
    }
    else if (load)
    {
        loader = post(([this, runNumber, &run] {
            run = this->m_db.getRun(runNumber);
            std::lock_guard<std::mutex> lock(this->m_lock);
            if (run)
            {
                Cache::mapped_type &item = m_cache[runNumber];
                item.ref = run;
                item.loader.reset();
            }
            else
            {
                this->m_cache.erase(runNumber);
                throw this->m_db.errorString();
            }
        }));
        m_cache[runNumber].loader = loader;
    }
    lock.unlock();
    if (load && loader)
    {
        loader->wait();
        return getRun(runNumber, false);
    }
    return RunInfo::Shared();
}

bool RFMFactory::removeRun(RunInfo::RunNumber runNumber)
{
    bool ret;
    RunInfo::Shared run;
    Worker::SharedTask task = post(([this, runNumber, &ret] {
        ret = this->m_db.removeRun(runNumber);
        if (ret)
        {
            std::lock_guard<std::mutex> lock(this->m_lock);
            this->m_cache.erase(runNumber);
        }
        else
            throw this->m_db.errorString();
    }));
    task->wait();
    if (ret)
        runRemoveSignal(runNumber);
    return ret;
}

void RFMFactory::release(const RunInfo &runInfo)
{
    /* don't create singleton if it doesn't exist, no need to use SingletonMutex
     * here, this is not likely to change after init */
    if (m_instance)
    {
        std::lock_guard<std::mutex> lock(m_instance->m_lock);
        m_instance->m_cache.erase(runInfo.runNumber());
    }
}

MergedFile::Shared RFMFactory::addFile(RunInfo &run)
{
    MergedFile::Shared ret;

    /* m_worker is thread safe, no need to lock here */
    Worker::SharedTask task = post([this, &run, &ret] {
        ret = this->m_db.insertMergedFile(run, run.getNextFileNumber());
        if (ret)
        {
            std::lock_guard<std::mutex> lock(run.m_lock);
            run.m_files.push_back(ret);
        }
        else
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    if (ret)
        fileAddSignal(ret);
    return ret;
}

MergedFileIndex::Shared RFMFactory::createIndex(RunInfo &run)
{
    MergedFileIndex::Shared ret;

    /* m_worker is thread safe, no need to lock here */
    Worker::SharedTask task = post([this, &run, &ret] {
        MergedFile::Shared file = this->m_db.insertMergedFile(
            run, MergedFile::IndexFileNumber);
        if (file)
        {
            ret = std::static_pointer_cast<MergedFileIndex>(file);
            std::lock_guard<std::mutex> lock(run.m_lock);
            run.m_index = ret;
        }
        else
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    if (ret)
        fileAddSignal(std::static_pointer_cast<MergedFile>(ret));
    return ret;
}

bool RFMFactory::getAllRuns(RFMFactory::RunNumberList &runs)
{
    Worker::SharedTask task = post([this, &runs] {
        if (!this->m_db.getAllRuns(runs))
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    return task->error() == RFMFactory::Task::OK;
}

bool RFMFactory::getMigratedRuns(RFMFactory::RunNumberList &runs)
{
    Worker::SharedTask task = post([this, &runs] {
        if (!this->m_db.getMigratedRuns(runs))
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    return task->error() == RFMFactory::Task::OK;
}

bool RFMFactory::getFileStats(FileStats &stats)
{
    Worker::SharedTask task = post([this, &stats] {
        if (!this->m_db.getFileStats(stats))
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    return task->error() == RFMFactory::Task::OK;
}

DaqInfo::Shared RFMFactory::getDaqInfo(DaqInfo::CrateId id) const
{
    std::map<DaqInfo::CrateId, DaqInfo::Shared>::const_iterator it = m_daqs.find(
        id);
    return (it != m_daqs.end()) ? it->second : DaqInfo::Shared();
}

RFMFactory::DaqInfoList RFMFactory::getDaqInfoList(
    const RunInfo::DaqsList &list) const
{
    DaqInfoList daqs;
    /* retrieve daqs */
    for (DaqInfo::CrateId id : list)
    {
        DaqInfo::Shared daq = getDaqInfo(id);
        if (!daq)
            throw RFMException("unknown DAQ " + std::to_string(id));
        daqs.push_back(std::move(daq));
    }
    return daqs;
}

RFMFactory::DaqInfoList RFMFactory::getDaqInfoList() const
{
    DaqInfoList daqs;
    for (const DaqInfoMap::value_type &v : m_daqs)
        daqs.push_back(v.second);
    return daqs;
}

void RFMFactory::loadEvents(MergedFile &file)
{
    /* no need to lock for that */
    if (file.m_events)
    {
        return;
    }
    std::unique_lock<std::mutex> lock(file.m_lock);

    if (!file.m_loadTask)
    {
        file.m_loadTask = post([this, &file] {
            std::size_t eventsSize;
            std::unique_ptr<MergedFile::EventsList> list(
                new MergedFile::EventsList);
            if (!this->m_db.getFileEvents(file, *list, eventsSize))
            {
                throw this->m_db.errorString();
            }
            std::lock_guard<std::mutex> lock(file.m_lock);
            file.m_events.swap(list);
            file.m_eventsSize = eventsSize;
        });
    }
    Worker::SharedTask task = file.m_loadTask;
    lock.unlock();
    task->wait();
}

FileEvent::Shared RFMFactory::addEvent(MergedFile &file,
                                       FileEvent::EventNumber seqNum,
                                       FileEvent::SequenceNumber eventNumber,
                                       std::size_t size)
{
    FileEvent::Shared ret;
    loadEvents(file);

    /* m_worker is thread safe, no need to lock here */
    Worker::SharedTask task = post([this, &file, &ret, eventNumber, seqNum,
                                    size] {
        ret = this->m_db.insertFileEvent(file, seqNum, eventNumber, size);
        if (ret)
        {
            std::lock_guard<std::mutex> lock(file.m_lock);
            if (!file.m_events)
                throw std::string("failed to add events on uninitialized file");

            file.m_events->push_back(ret);
            file.m_eventsSize += size;
        }
        else
            throw this->m_db.errorString();
    });
    task->wait();
    if (ret)
        eventAddSignal(ret);
    return ret;
}

bool RFMFactory::getNextRetry(RunInfo::RunNumber &runNumber,
                              MergedFile::FileNumber &fileNumber,
                              std::time_t &date)
{
    Worker::SharedTask task = post([this, &runNumber, &fileNumber, &date] {
        if (!this->m_db.getNextRetry(runNumber, fileNumber, date))
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    return task->error() == RFMFactory::Task::OK;
}

bool RFMFactory::getNextExpiry(RunInfo::RunNumber &runNumber, std::time_t &date)
{
    Worker::SharedTask task = post([this, &runNumber, &date] {
        if (!this->m_db.getNextExpiry(runNumber, date))
        {
            throw this->m_db.errorString();
        }
    });
    task->wait();
    return task->error() == RFMFactory::Task::OK;
}

Worker::SharedTask RFMFactory::post(const std::function<void()> &funTask)
{
    bool success = false;
    Worker::SharedTask ret;
    while (!success)
    {
        try
        {
            ret = m_worker.post(funTask);
            success = true;
        }
        catch (NTOFException &ex)
        {
            LOG(WARNING) << "[RFMFactory] workqueue full, waiting...";
            std::this_thread::sleep_for(WorkQueueFullDelay);
        }
    }
    return ret;
}
