/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T13:10:57+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "Archiver.hpp"

#include <functional>
#include <map>

#include <easylogging++.h>

#include "Config.h"
#include "archiver/FakeArchiver.hpp"
#include "archiver/LocalArchiver.hpp"
#include "archiver/PreAnalysisArchiver.hpp"
#include "archiver/XRootArchiver.hpp"

#include <Singleton.hxx>

using namespace ntof::rfm;

template class ntof::utils::Singleton<Archiver>;

typedef std::map<std::string, std::function<Archiver *()>> ArchiverRegistry;
/*
 * since we use static libraries this method is safer than self-registering
 * objects, those would be dropped at link time
 */
static const ArchiverRegistry s_registry = {
    {"FakeArchiver", [] { return new FakeArchiver(); }},
    // support old configs
    {"CastorArchiver", [] { return new XRootArchiver(); }},
    {"XRootArchiver", [] { return new XRootArchiver(); }},
    {"LocalArchiver", [] { return new LocalArchiver(); }},
    {"PreAnalysisArchiver", [] { return new PreAnalysisArchiver(); }}};

template<>
Archiver &ntof::utils::Singleton<Archiver>::instance()
{
    if (!m_instance)
    {
        SingletonMutex lock;
        // cppcheck-suppress identicalInnerCondition ; MT protection
        if (!m_instance)
        {
            const std::string &archiver = Config::instance().getArchiverName();
            ArchiverRegistry::const_iterator it = s_registry.find(archiver);
            if (it == s_registry.end())
            {
                LOG(WARNING) << "Archiver " << archiver << " not found, "
                             << " falling back on FakeArchiver";
                m_instance = new FakeArchiver();
            }
            else
                m_instance = it->second();
        }
    }
    return *m_instance;
}

void Archiver::finishFile(const boost::filesystem::path &) {}

void Archiver::finishRun(const boost::filesystem::path &) {}
