/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-18T09:13:27+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "RFMCli.hpp"

#include <DIMAck.h>
#include <DIMException.h>
#include <DIMSuperClient.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Config.h"
#include "RFMMonitoring.hpp"

using namespace ntof::rfm;
using namespace ntof::dim;

RFMCli::RFMCli() : m_cmd(Config::instance().getDimPrefix()) {}

bool RFMCli::cmdStart(RunInfo::RunNumber runNumber,
                      const DaqInfo::CrateIdList &daqs,
                      const std::string &experiment,
                      const bool *approved,
                      const bool *standalone)
{
    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name").set_value("runStart");
    cmd.append_attribute("runNumber").set_value(runNumber);

    for (DaqInfo::CrateId daq : daqs)
    {
        pugi::xml_node daqNode = cmd.append_child("daq");
        daqNode.append_attribute("crateId").set_value(daq);
    }

    if (!experiment.empty())
        cmd.append_attribute("experiment").set_value(experiment.c_str());

    if (approved)
        cmd.append_attribute("approved").set_value(*approved ? 1 : 0);

    if (standalone)
        cmd.append_attribute("standalone").set_value(*standalone ? 1 : 0);

    try
    {
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(DEBUG) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::cmdStop(RunInfo::RunNumber runNumber)
{
    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name").set_value("runStop");
    cmd.append_attribute("runNumber").set_value(runNumber);

    try
    {
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::cmdEvent(RunInfo::RunNumber runNumber,
                      FileEvent::SequenceNumber seq,
                      FileEvent::EventNumber event,
                      std::size_t size)
{
    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name").set_value("event");
    cmd.append_attribute("runNumber").set_value(runNumber);
    cmd.append_attribute("validatedEvent").set_value(seq);
    cmd.append_attribute("timingEvent").set_value(event);
    cmd.append_attribute("fileSize").set_value(size);

    try
    {
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::cmdFileIgnore(RunInfo::RunNumber runNumber,
                           MergedFile::FileNumber fileNumber,
                           bool ignore)
{
    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name").set_value("fileIgnore");
    cmd.append_attribute("runNumber").set_value(runNumber);
    cmd.append_attribute("fileNumber").set_value(fileNumber);
    cmd.append_attribute("ignore").set_value(ignore ? "1" : "0");

    try
    {
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::setRunExpiry(RunInfo::RunNumber runNumber, std::time_t expiry)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Runs");
    try
    {
        DIMData::List data;
        data.emplace_back(RFMMonitoring::RP_RUN_NUMBER, uint32_t(runNumber));
        data.emplace_back(RFMMonitoring::RP_EXPIRYDATE, uint32_t(expiry));
        DIMAck ack = cli.setParameter(runNumber, data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::setRunExperiment(RunInfo::RunNumber runNumber,
                              const std::string &experiment)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Runs");
    try
    {
        DIMData::List data;
        data.emplace_back(RFMMonitoring::RP_RUN_NUMBER, uint32_t(runNumber));
        data.emplace_back(RFMMonitoring::RP_EXPERIMENT, experiment);
        DIMAck ack = cli.setParameter(runNumber, data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool RFMCli::setRunApproved(RunInfo::RunNumber runNumber)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Runs");
    try
    {
        DIMData::List data;
        data.emplace_back(RFMMonitoring::RP_RUN_NUMBER, uint32_t(runNumber));
        data.emplace_back(RFMMonitoring::RP_APPROVED, true);
        DIMAck ack = cli.setParameter(runNumber, data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

void RFMCli::listen()
{
    if (m_runsCli)
        return;

    m_runsCli.reset(
        new DIMParamListClient(Config::instance().getDimPrefix() + "/Runs"));
    m_runsCli->dataSignal.connect(runsSignal);

    m_currentCli.reset(
        new DIMParamListClient(Config::instance().getDimPrefix() + "/Current"));
    m_currentCli->dataSignal.connect(currentSignal);

    m_filesCli.reset(new DIMDataSetClient(Config::instance().getDimPrefix() +
                                          "/Current/Files"));
    m_filesCli->dataSignal.connect(filesSignal);
}

std::vector<DIMData> RFMCli::getRunsInfo() const
{
    if (m_runsCli)
        return m_runsCli->getParameters();
    return std::vector<DIMData>();
}

std::vector<DIMData> RFMCli::getCurrentFiles() const
{
    if (m_filesCli)
        return m_filesCli->getLatestData();
    return std::vector<DIMData>();
}

std::vector<DIMData> RFMCli::getCurrentRun() const
{
    if (m_currentCli)
        return m_currentCli->getParameters();
    return std::vector<DIMData>();
}

std::vector<DIMData> RFMCli::getFiles(RunInfo::RunNumber run) const
{
    DIMSuperClient cli(Config::instance().getDimPrefix() + "/Files", true);
    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name") = "listFiles";
    cmd.append_attribute("runNumber") = run;

    DIMAck ack = cli.sendCommand(doc);
    if (DIMAck::OK != ack.getStatus())
    {
        LOG(INFO) << "Command failed: " << ack.getMessage();
        return std::vector<DIMData>();
    }

    std::vector<DIMData> ret;
    for (pugi::xml_node node : ack.getRoot().children())
    {
        ret.emplace_back(node);
    }
    return ret;
}
