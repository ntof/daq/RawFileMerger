
include_directories(
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_BINARY_DIR}
    ${CMAKE_CURRENT_BINARY_DIR})
include_directories(SYSTEM
    ${DIM_INCLUDE_DIRS}
    ${NTOFUTILS_INCLUDE_DIRS}
    ${SQLITE_INCLUDE_DIRS}
    ${XROOTD_INCLUDE_DIRS}
    ${ZLIB_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${PUGI_INCLUDE_DIRS})

set(SRC
  RFMCommand.cpp RFMCommand.hpp
  RFMDatabase.cpp RFMDatabase.hpp
  RFMFactory.cpp RFMFactory.hpp
  RFMException.cpp RFMException.hpp
  RFMManager.cpp RFMManager.hpp
  RFMMonitoring.cpp RFMMonitoring.hpp
  RFMMonitoringHandlers.cpp RFMMonitoringHandlers.hpp
  RFMUtils.cpp RFMUtils.hpp
  FileDeleter.cpp FileDeleter.hpp
  FileUtils.cpp FileUtils.hpp
  IndexerTask.cpp IndexerTask.h
  MergerTask.cpp MergerTask.h
  RawHeaders.h
  Config.h Config.cpp
  Paths.hpp Paths.cpp
  data/DaqInfo.hpp data/DaqInfo.cpp
  data/MergedFile.hpp data/MergedFile.cpp
  data/MergedFileIndex.hpp data/MergedFileIndex.cpp
  data/FileEvent.hpp data/FileEvent.cpp
  data/RunInfo.hpp data/RunInfo.cpp
  data/Storable.hpp data/Storable.hxx
  Archiver.hpp Archiver.cpp
  SqliteHelper.hpp SqliteHelper.cpp
  RFMLockFile.hpp RFMLockFile.cpp
  Regex.hpp Regex.cpp

  archiver/FakeArchiver.hpp archiver/FakeArchiver.cpp
  archiver/LocalArchiver.hpp archiver/LocalArchiver.cpp
  archiver/XRootArchiver.hpp archiver/XRootArchiver.cpp
  archiver/PreAnalysisArchiver.hpp archiver/PreAnalysisArchiver.cpp

  listener/DaqListener.cpp listener/DaqListener.hpp
  listener/EACSListener.cpp listener/EACSListener.hpp)

add_library(RFMergerLib STATIC ${SRC})
target_link_libraries(RFMergerLib
  ntofutils ${DIM_LIBRARIES} ${Boost_LIBRARIES}
  ${SQLITE_LIBRARIES}
  ${XROOTD_LIBRARIES} ${ZLIB_LIBRARIES} ${PUGI_LIBRARIES})
add_dependencies(RFMergerLib DIM ntofutils)

add_library(RFMCliLib STATIC RFMCli.hpp RFMCli.cpp)
target_link_libraries(RFMCliLib RFMergerLib
ntofutils ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${PUGI_LIBRARIES})
add_dependencies(RFMergerLib DIM ntofutils)

add_executable(RawFileMergerCli RFMCli_main.cpp RFMCli_import.cpp)
target_link_libraries(RawFileMergerCli RFMCliLib  ntofutils
  ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${PUGI_LIBRARIES})

add_executable(RawFileMerger Main.cpp)
target_link_libraries(RawFileMerger RFMergerLib)

install(TARGETS RawFileMerger
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT runtime)
install(TARGETS RawFileMergerCli
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT cli)
