/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T11:31:22+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "Paths.hpp"

#include <time.h>

#include "Config.h"
#include "RFMException.hpp"
#include "RFMFactory.hpp"
#include "data/DaqInfo.hpp"
#include "data/FileEvent.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

static std::string rawFileName(const FileEvent &event)
{
    std::ostringstream oss;
    oss << "run" << event.runNumber() << "_" << event.eventNumber()
        << "_s1.raw";
    return oss.str();
}

static std::string eventFileName(const FileEvent &event)
{
    std::ostringstream oss;
    oss << "run" << event.runNumber() << "_" << event.eventNumber() << ".event";
    return oss.str();
}

static DaqInfo::Shared getStandaloneDaq(const RunInfo &run)
{
    RunInfo::DaqsList daqs = run.getDaqs();
    if (daqs.empty())
    {
        throw RFMException("Standalone run has no DAQ");
    }
    DaqInfo::Shared daq = RFMFactory::instance().getDaqInfo(*daqs.begin());
    if (!daq)
    {
        throw RFMException("Standalone run is running on unknown DAQ");
    }
    return daq;
}

bfs::path Paths::getPath(const RunInfo &run)
{
    if (run.isStandalone())
    {
        return Paths::getPath(*getStandaloneDaq(run), run);
    }
    else
    {
        return bfs::path(Config::instance().getPaths().local) /
            ("run" + std::to_string(run.runNumber()));
    }
}

bfs::path Paths::getPath(const DaqInfo &daq, const RunInfo &run)
{
    return bfs::path(Config::instance().getPaths().mount) / daq.name() /
        ("run" + std::to_string(run.runNumber()));
}

bfs::path Paths::getRawFilePath(const DaqInfo &daq,
                                const RunInfo &run,
                                const FileEvent &event)
{
    return Paths::getPath(daq, run) / rawFileName(event);
}

bfs::path Paths::getEventFilePath(const RunInfo &run, const FileEvent &event)
{
    if (run.isStandalone())
    {
        return Paths::getPath(*getStandaloneDaq(run), run) /
            eventFileName(event);
    }
    else
    {
        return bfs::path(Config::instance().getPaths().local) /
            ("run" + std::to_string(run.runNumber())) / eventFileName(event);
    }
}

bfs::path Paths::getRunFilePath(const RunInfo &run)
{
    return getPath(run) / ("run" + std::to_string(run.runNumber()) + ".run");
}

bfs::path Paths::getRemotePath(const RunInfo &run)
{
    switch (Config::instance().getRemoteSubdirs())
    {
    case Config::RemoteSubdirs::NONE:
        return bfs::path(Config::instance().getPaths().remote);
    default:
    case Config::RemoteSubdirs::FULL:
        return bfs::path(Config::instance().getPaths().remote) /
            std::to_string(run.year()) / run.expArea() / run.experiment() /
            std::to_string(run.runNumber());
    case Config::RemoteSubdirs::RUN:
        return bfs::path(Config::instance().getPaths().remote) /
            std::to_string(run.runNumber());
    }
}

bfs::path Paths::getRemotePath(const RunInfo &run, const MergedFile &file)
{
    if (Config::instance().getRemoteSubdirs() == Config::RemoteSubdirs::FULL)
    {
        return getRemotePath(run) /
            ((file.fileNumber() == MergedFile::IndexFileNumber) ? "stream0" :
                                                                  "stream1");
    }
    return getRemotePath(run);
}

bfs::path Paths::getRemoteFilePath(const RunInfo &run, const MergedFile &file)
{
    return getRemotePath(run, file) / file.fileName();
}
