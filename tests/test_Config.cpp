//
// Created by matteof on 01/07/2020.
//

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "ConfigMisc.h"
#include "NTOFException.h"
#include "local-config.h"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof;

class TestConfig : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfig);
    CPPUNIT_TEST(test_Config_Exceptions);
    CPPUNIT_TEST(test_FullConfig);
    CPPUNIT_TEST(test_PreAnalysisConfig);
    CPPUNIT_TEST_SUITE_END();

protected:
    bfs::path dataDir_;

public:
    void setUp() { dataDir_ = bfs::path(SRCDIR) / "tests" / "data"; }

    void tearDown()
    {
        Config::load((dataDir_ / "conf.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
    }

    void test_Config_Exceptions()
    {
        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "no_existing_file.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "conf_no_castor_node.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "conf_wrong_replace_strategy").string(),
                         (dataDir_ / "configMisc.xml").string()),
            NTOFException);
    }

    void test_FullConfig()
    {
        Config::load((dataDir_ / "conf_full.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
        Config &conf = Config::instance();
        CPPUNIT_ASSERT_EQUAL(std::string("/ntof/local/path"),
                             conf.getPaths().local);
        CPPUNIT_ASSERT_EQUAL(std::string("/ntof/remote/path"),
                             conf.getPaths().remote);
        CPPUNIT_ASSERT_EQUAL(std::string("/mnt/path"), conf.getPaths().mount);
        CPPUNIT_ASSERT_EQUAL(std::string("castorpublic"), conf.getCastorNode());
        CPPUNIT_ASSERT_EQUAL(std::string("svc_class"), conf.getSvcClass());
        CPPUNIT_ASSERT_EQUAL(std::string("eos.space=spinners"),
                             conf.getCastorOptions());
        CPPUNIT_ASSERT_EQUAL((uint64_t) 2147483648, conf.getMinTotalSize());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 10, conf.getMaxEvents());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 3600, conf.getDefaultExpiry());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 1, conf.getPoolSize());
        CPPUNIT_ASSERT_EQUAL((uint64_t) 256, conf.getBufferSize());
        CPPUNIT_ASSERT_EQUAL(Config::ReplaceStrategy::RENAME,
                             conf.getReplaceStrategy());
        CPPUNIT_ASSERT_EQUAL(std::string("RENAME"),
                             Config::toString(conf.getReplaceStrategy()));
        CPPUNIT_ASSERT_EQUAL(std::string("NONE"),
                             Config::toString(conf.getListeningMode()));
        CPPUNIT_ASSERT_EQUAL(std::string("CASTORPREFIX"), conf.getDimPrefix());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 20, conf.getDimHistoryCount());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 50, conf.getDimErrorsCount());
        ASSERT(conf.getPreAnalysisConfig() == nullptr);
    }

    void test_PreAnalysisConfig()
    {
        Config::load((dataDir_ / "conf_preanalysis.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
        Config &conf = Config::instance();
        const Config::PreAnalysis *pre = conf.getPreAnalysisConfig();
        if (pre == nullptr)
            throw "pre is null";

        EQ(std::string("/bin/echo"), pre->cmd);
        EQ(std::size_t(5), pre->skipCount);
        EQ(false, pre->checkErrorCode);
        EQ(true, pre->onlyNew);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfig);
