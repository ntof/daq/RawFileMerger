/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-05T08:50:21+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>
#include <fstream>

#include <boost/filesystem.hpp>

#include <DIMXMLService.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Archiver.hpp"
#include "Config.h"
#include "Env.hpp"
#include "archiver/LocalArchiver.hpp"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestLocalArchiver : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestLocalArchiver);
    CPPUNIT_TEST(fileManagement);
    CPPUNIT_TEST(fileWrite);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_path;

public:
    void setUp() override { m_path = envInit("TestLocalArchiver"); }

    void tearDown() override { envDestroy(m_path); }

    void fileManagement()
    {
        LocalArchiver archiver;
        archiver.createDir(m_path / "test");
        EQ(true, archiver.getInfo(m_path / "test").exists);

        const bfs::path file = m_path / "test" / "file.txt";
        std::ofstream(file.string()) << "test";

        /* check file exists and content */
        Archiver::FileInfo info = archiver.getInfo(file);
        EQ(true, info.exists);
        EQ(uint64_t(4), info.fileSize);
        EQ(true, archiver.isMigrated(file)); // local files are always migrated

        /* renaming and moving */
        archiver.renameFile(file, m_path / "temp.txt");
        EQ(true, archiver.getInfo(m_path / "temp.txt").exists);
        EQ(false, archiver.getInfo(file).exists);
        // file that doesn't exist are not migrated
        EQ(false, archiver.isMigrated(file));
        archiver.renameFile(m_path / "temp.txt", file);

        // renaming errors
        ASSERT_THROW(archiver.renameFile(m_path / "temp.txt", file),
                     Archiver::Exception);
        EQ(uint64_t(4), archiver.getInfo(file).fileSize); // left untouched

        /* can't create directory on a file */
        ASSERT_THROW(archiver.createDir(file), Archiver::Exception);

        archiver.deleteFile(file);
        archiver.deleteFile(file); // removing a file that doesn't exist is fine
        // files only
        ASSERT_THROW(archiver.deleteFile(m_path / "test"), Archiver::Exception);
        EQ(false, archiver.getInfo(file).exists);
    }

    void fileWrite()
    {
        LocalArchiver archiver;
        const bfs::path fileName = m_path / "file.bin";
        Archiver::File::Shared file = archiver.open(fileName);
        EQ(true, bool(file));
        file->write(0, 4, "test");
        // seeking in file
        file->write(8, 4, "end0");
        file->write(4, 4, "midd");
        file->close();
        EQ(uint64_t(12), archiver.getInfo(fileName).fileSize);

        std::string text;
        std::ifstream(fileName.string()) >> text;
        EQ(std::string("testmiddend0"), text);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestLocalArchiver);
