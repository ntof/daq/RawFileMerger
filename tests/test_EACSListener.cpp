/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-16T21:22:39+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMDataSet.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Config.h"
#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::dim;

class TestEACSListener : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestEACSListener);
    CPPUNIT_TEST(listeningMode);
    CPPUNIT_TEST(managedRun);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestEACSListener");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
        Config::destroy();
    }

    void sendEvents(DIMDataSet &srv)
    {
        for (int i = 0; i < 5; ++i)
        {
            DIMData::Index idx = 0;
            srv.addData<uint32_t>(idx++, "runNumber", "", 901234,
                                  AddMode::OVERRIDE, false);
            srv.addData<uint32_t>(idx++, "eventNumber", "", i,
                                  AddMode::OVERRIDE, false);
            srv.addData<uint32_t>(idx++, "validatedNumber", "", i,
                                  AddMode::OVERRIDE, false);
            srv.addData<uint64_t>(idx++, "fileSize", "", 1024,
                                  AddMode::OVERRIDE, true);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    void listeningMode()
    {
        DIMDataSet srv("EACS/Validation");
        Config::instance().setListeningMode(Config::ListeningMode::EACS);
        RFMManager mgr;
        mgr.initialize();
        mgr.start();

        { /* wait for manager to be ready before sending events */
            RFMCli cli;
            cli.listen();
            waitFor([&cli]() { return !cli.getCurrentRun().empty(); },
                    "RFMManager did not mount");
        }

        sendEvents(srv);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        RunInfo::Shared run = RFMFactory::instance().getRun(901234);
        EQ(true, bool(run));
        EQ(false, run->hasStopDate());

        MergedFile::Shared file = run->currentFile();
        EQ(true, bool(file));
        EQ(size_t(5 * 1024), file->eventsSize());
        EQ(size_t(5), file->eventsCount());

        {
            /* send an event for another run */
            srv.addData<uint32_t>(0, "runNumber", "", 901235, AddMode::OVERRIDE,
                                  true);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        EQ(true, run->hasStopDate());
    }

    void managedRun()
    {
        DIMDataSet srv("EACS/Validation");
        Config::instance().setListeningMode(Config::ListeningMode::NONE);
        RFMManager mgr;
        mgr.initialize();
        mgr.start();

        { /* wait for manager to be ready before sending events */
            RFMCli cli;
            cli.listen();
            waitFor([&cli]() { return !cli.getCurrentRun().empty(); },
                    "RFMManager did not mount");
        }

        sendEvents(srv);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        RunInfo::Shared run = RFMFactory::instance().getRun(901234);
        EQ(false, bool(run)); /* not listening */
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEACSListener);
