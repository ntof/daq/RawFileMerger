/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-11-18T09:50:02+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <iostream>
#include <thread>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "archiver/XRootArchiver.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestXRootArchiver : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestXRootArchiver);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_dir;

public:
    void setUp() override
    {
        const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
        Config::load((dataDir / "conf_integ.xml").string(),
                     (dataDir / "configMisc.xml").string());
        m_dir = bfs::path(Config::instance().getPaths().remote);
    }

    void tearDown() override {}

    void simple()
    {
        XRootArchiver archiver;
        archiver.createDir(m_dir);

        Archiver::File::Shared file = archiver.open(m_dir / "test.txt");
        EQ(true, bool(file));

        const std::size_t buffCount = 1024 * 2;   // 2GB transfer
        const std::size_t buffSize = 1024 * 1024; // 1MB buffers
        {
            std::vector<char> buffer(buffSize, 0);
            for (std::size_t idx = 0; idx < buffCount; ++idx)
                file->write(idx * buffer.size(), buffer.size(), buffer.data());

            /* does crc check */
            file->close();
            file.reset();
        }

        Archiver::FileInfo info = archiver.getInfo(m_dir / "test.txt");
        EQ(true, bool(info));
        EQ(true, info.exists);
        EQ(uint64_t(buffCount * buffSize), info.fileSize);

        while (!archiver.isMigrated(m_dir / "test.txt"))
        {
            std::cout << "Waiting for file to be migrated" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }

        archiver.renameFile(m_dir / "test.txt", m_dir / "test2.txt");
        EQ(true, bool(archiver.getInfo(m_dir / "test2.txt")));

        archiver.deleteFile(m_dir / "test2.txt");
        EQ(false, bool(archiver.getInfo(m_dir / "test2.txt")));
    }

    void mkdir()
    {
        XRootArchiver archiver;
        archiver.createDir(m_dir / "test" / "test");
        /* already exists */
        archiver.createDir(m_dir / "test" / "test");
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestXRootArchiver);
