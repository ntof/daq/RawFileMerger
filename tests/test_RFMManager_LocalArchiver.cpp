/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-05T13:03:45+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <DaqTypes.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <fcntl.h>

#include "Archiver.hpp"
#include "Config.h"
#include "Env.hpp"
#include "Paths.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "archiver/LocalArchiver.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerLocalArchiver : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerLocalArchiver);
    CPPUNIT_TEST(config);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(perf);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerLocalArchiver");
        {
            Config &c(Config::instance());
            Config::Paths &paths = const_cast<Config::Paths &>(c.getPaths());
            paths.remote = (m_path / "out").string();

            const_cast<std::string &>(c.getArchiverName()) = "LocalArchiver";
            Archiver::destroy();
        }
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
        {
            /* restore configuration and archiver */
            Config &c(Config::instance());
            const_cast<std::string &>(c.getArchiverName()) = "FakeArchiver";
            Archiver::destroy();
        }
    }

    void config()
    {
        /* ensure archiver can load */
        EQ(LocalArchiver::NAME, Archiver::instance().name());
    }

    void simple()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        RFMCli cli;
        cli.listen();

        bool approve = true;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", &approve));

        EQ(true,
           cli.cmdEvent(901234, 0, 0, (AcquisitionHeader::SIZE + 2048) * 2));
        EQ(true,
           cli.cmdEvent(901234, 1, 1, (AcquisitionHeader::SIZE + 2048) * 2));

        EQ(true, cli.cmdStop(901234));

        { // wait for RunInfo to show latest info
            RunInfoWaiter waiter;
            DIMData info;
            EQ(true,
               waiter.wait(
                   [&info](DIMData::List &data) {
                       info = DIMData(0, data);
                       DIMData migrated = getData("901234.migratedCount", info);
                       return (migrated.getULongValue() ==
                               2); // don't forget the index
                   },
                   3000));
        }

        RunInfo::Shared info(RFMFactory::instance().getRun(901234));
        EQ(true, bool(info));
        EQ(true, info->hasFiles());

        const bfs::path rawFile = Paths::getRemoteFilePath(
            *info, *info->getFiles().front());

        EQ(true, bfs::exists(rawFile));
        /* (RCTR,MODH + (EVEH+ADDH)*2 + ACQC*4) */
        EQ(uint64_t((264 + (168 * 2) + (2076 * 4))), bfs::file_size(rawFile));
    }

    std::chrono::duration<double, std::milli> perfReference(
        const std::vector<char> &buffer,
        std::size_t count)
    {
        auto start = std::chrono::high_resolution_clock::now();
        for (std::size_t i = 0; i < count; ++i)
        {
            int fd = ::open((m_path / "test1").string().c_str(),
                            O_WRONLY | O_TRUNC | O_CREAT, 0644);
            ::write(fd, buffer.data(), buffer.size());
            ::close(fd);
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "Reference write time: " << elapsed.count()
                  << " milliseconds" << std::endl;
        return elapsed;
    }

    void perf()
    {
        std::vector<char> buffer(1024 * 1024 * 100);
        std::size_t count = 10;

        perfReference(buffer, count); // turn on spinner drives
        std::chrono::duration<double, std::milli> reference = perfReference(
            buffer, count);

        /* single write */
        LocalArchiver archiver;
        auto start = std::chrono::high_resolution_clock::now();
        for (std::size_t i = 0; i < count; ++i)
        {
            Archiver::File::Shared file = archiver.open(m_path / "test2");
            file->write(0, buffer.size(), buffer.data());
            file->close();
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end - start;
        std::cout << "LocalArchiver (single) write time: " << elapsed.count()
                  << " milliseconds" << std::endl;
        DBL_EQ(reference.count(), elapsed.count(), reference.count() / 1.5);

        /* small chunks write */
        start = std::chrono::high_resolution_clock::now();
        for (std::size_t i = 0; i < count; ++i)
        {
            Archiver::File::Shared file = archiver.open(m_path / "test2");
            const std::size_t chunkSize = 512;
            for (std::size_t i = 0; i < buffer.size(); i += chunkSize)
            {
                file->write(i, chunkSize, buffer.data() + i);
            }
            file->close();
        }
        end = std::chrono::high_resolution_clock::now();
        elapsed = end - start;
        std::cout << "LocalArchiver (chunked) write time: " << elapsed.count()
                  << " milliseconds" << std::endl;
        DBL_EQ(reference.count(), elapsed.count(),
               reference.count() *
                   3.5); // local is within * 1 margin but CI has problems
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerLocalArchiver);
