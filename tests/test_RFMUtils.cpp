/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-06T11:19:41+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "RFMUtils.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

using namespace ntof::rfm;

class TestRFMUtils : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMUtils);
    CPPUNIT_TEST(vstreambuf);
    CPPUNIT_TEST_SUITE_END();

public:
    void vstreambuf()
    {
        std::vector<char> buffer(10, 0);
        class vstreambuf sb(buffer);
        std::iostream stream(&sb);

        stream << "1234";
        std::string text;
        stream >> text;
        EQ(std::string("1234"), std::string(text.c_str()));

        EQ(true, stream.eof());
        EQ(false, stream.fail());

        stream.clear();

        stream << "567890";
        EQ(false, stream.fail());

        stream << "0";
        EQ(true, stream.fail());

        stream.clear();
        stream.seekg(0);

        stream >> text;
        EQ(std::string("1234567890"), text);

        EQ(std::streambuf::pos_type(6), sb.seekoff(-4, std::ios_base::end));
        EQ(std::streambuf::pos_type(6), sb.seekpos(6));
        EQ(std::streambuf::pos_type(6), sb.seekoff(0, std::ios_base::cur));

        EQ(std::streambuf::pos_type(-1), sb.seekoff(10, std::ios_base::cur));
        EQ(std::streambuf::pos_type(-1), sb.seekoff(-1, std::ios_base::beg));
        EQ(std::streambuf::pos_type(-1), sb.seekpos(buffer.size() + 1));
        EQ(std::streambuf::pos_type(-1), sb.seekpos(-1));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMUtils);
