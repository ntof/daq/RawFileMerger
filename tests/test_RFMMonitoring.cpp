/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-08T10:02:07+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Config.h"
#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMManager.hpp"
#include "RFMMonitoring.hpp"
#include "data/RunInfo.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMMonitoring : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMMonitoring);
    CPPUNIT_TEST(currentFilesUpdate);
    CPPUNIT_TEST(infoUpdate);
    CPPUNIT_TEST(currentRunEdit);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMMonitoring");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    void currentFilesUpdate()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        RFMCli cli;

        bool approve = true;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", &approve));

        EQ(true, cli.cmdEvent(901234, 0, 0, 1024));

        CurrentFilesWaiter waiter;
        EQ(true,
           waiter.wait(
               [](DIMData::List &data) {
                   DIMData status = getData("files.0.status", DIMData(0, data));
                   return status.getStringValue() == "incomplete";
               },
               3000));

        EQ(true, cli.cmdStop(901234));
        EQ(true, cli.setRunApproved(901234));
        EQ(true,
           waiter.wait(
               [](DIMData::List &data) {
                   DIMData status = getData("files.0.status", DIMData(0, data));
                   return status.getStringValue() == "migrated";
               },
               3000));

        { /* let's wait for the index */
            DIMData info;
            EQ(true,
               waiter.wait(
                   [&info](DIMData::List &data) {
                       info = DIMData(0, data);
                       DIMData status = getData("index.status", info);
                       return status.getStringValue() == "migrated";
                   },
                   3000));
            EQ(std::string("run901234.idx.finished"),
               getData("index.fileName", info).getStringValue());
        }
    }

    void infoUpdate()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(100);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        DimInfoWaiter waiter("MERGER/Info", 0);
        EQ(true, waiter.waitUpdate(1));

        /* ensure that we're not ticking on every loop */
        EQ(false, waiter.waitUpdate(2, 200));

        RFMCli cli;

        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", nullptr));
        EQ(true, cli.cmdEvent(901234, 0, 0, 1024));

        EQ(true, waiter.waitUpdate(2, 200));
    }

    void currentRunEdit()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(100);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        RFMCli cli;
        CurrentRunWaiter waiter;

        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", nullptr));
        EQ(true, cli.cmdEvent(901234, 0, 0, 1024));
        EQ(true,
           waiter.wait([](DIMData::List &data) { return !data.empty(); }, 3000));

        ntof::dim::DIMParamListClient params("MERGER/Current");
        std::vector<DIMData> args;
        args.emplace_back(RFMMonitoring::CRP_RUN_NUMBER,
                          RunInfo::RunNumber(901234));
        args.emplace_back(RFMMonitoring::CRP_EXPERIMENT, std::string("test"));
        args.emplace_back(RFMMonitoring::CRP_APPROVED, true);
        params.sendParameters(args);

        DIMData runData;
        EQ(true,
           waiter.wait(
               [&runData](DIMData::List &data) {
                   runData = DIMData(0, data);
                   DIMData exp = getData("experiment", runData);
                   return exp.getStringValue() == "test";
               },
               3000));
        EQ(true, getData("approved", runData).getBoolValue());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMMonitoring);
