/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-12T14:34:22+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>

#include <boost/filesystem.hpp>

#include <DaqTypes.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <fcntl.h>

#include "Archiver.hpp"
#include "Config.h"
#include "Env.hpp"
#include "IndexerTask.h"
#include "MergerTask.h"
#include "RFMFactory.hpp"
#include "archiver/LocalArchiver.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestIndexerTask : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestIndexerTask);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;
    std::size_t m_eventCount;

public:
    void setUp() override
    {
        m_path = envInit("TestIndexerTask");
        {
            Config &c(Config::instance());
            Config::Paths &paths = const_cast<Config::Paths &>(c.getPaths());
            paths.remote = (m_path / "out").string();

            const_cast<std::string &>(c.getArchiverName()) = "LocalArchiver";
            Archiver::destroy();
        }
        m_eventCount = 4;
        genFiles(m_path, m_eventCount);
        m_dim.reset(new DimTestHelper());
        /* ensure archiver can load */
        EQ(LocalArchiver::NAME, Archiver::instance().name());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
        {
            /* restore configuration and archiver */
            Config &c(Config::instance());
            const_cast<std::string &>(c.getArchiverName()) = "FakeArchiver";
            Archiver::destroy();
        }
    }

    void checkFile(const bfs::path &file)
    {
        std::ifstream ifs;
        ifs.open(file.string());
        {
            RunControlHeader rctr;
            ifs >> rctr;
            EQ(true, bool(ifs));
            EQ(uint32_t(901234), rctr.runNumber);
            EQ(uint32_t(0), rctr.segmentNumber);
            // EQ(uint32_t(1), rctr.streamNumber);
            EQ(ExperimentArea::LAB.name, rctr.getExperiment());
            {
                RunControlHeader temp;
                temp.updateDateTime();
                EQ(temp.date, rctr.date);
                // time generated with genFiles, not checking
            }
            EQ(uint32_t(2), rctr.totalNumberOfStreams);
            EQ(uint32_t(2), rctr.totalNumberOfChassis);
            EQ(uint32_t(2), rctr.totalNumberOfModules);
            EQ((std::vector<uint32_t>{1, 1}), rctr.numberOfChannelsPerModule);
        }
        {
            ModuleHeader modh;
            ifs >> modh;
            EQ(true, bool(ifs));
            EQ(std::size_t(2), modh.channelsConfig.size());
            ChannelConfig &cfg(modh.channelsConfig.at(0));
            EQ(std::string("TES1"), cfg.getDetectorType());
            EQ(uint32_t(0x01010000), cfg.str_mod_crate);
            EQ(std::string("ACQC"), cfg.getModuleType());
            EQ(uint32_t(0), cfg.detectorId);
            DBL_EQ(1000, cfg.sampleRate, 0.01);
            EQ(uint32_t(8000), cfg.sampleSize);
            DBL_EQ(100, cfg.fullScale, 0.01);
            EQ(std::string("INTC"), cfg.getClockState());
        }

        {
            IndexHeader indx;
            ifs >> indx;
            EQ(true, bool(ifs));
            EQ(m_eventCount, indx.indexes.size());
            for (size_t i = 0; i < m_eventCount; ++i)
            {
                IndexValue &value = indx.indexes.at(i);
                EQ(uint32_t(1), value.stream);
                EQ(uint32_t(1), value.segment);
                EQ(uint32_t(i), value.validatedNumber);
                EQ(uint32_t(i), value.eventNumber);
                /* RCTR + MODH + (EVEH + ADDH + ACQC*2) * i */
                EQ(uint64_t(264 + (168 + 2076 * 2) * i), value.offset);
            }
        }
        for (std::size_t i = 0; i < m_eventCount; ++i)
        {
            {
                EventHeader eveh;
                ifs >> eveh;
                EQ(true, bool(ifs));
                EQ(uint32_t(i), eveh.eventNumber);
                EQ(uint32_t(901234), eveh.runNumber);
                {
                    EventHeader temp;
                    temp.updateDateTime(std::time(nullptr));
                    EQ(temp.date, eveh.date);
                }
                EQ(int(EventHeader::PARASITIC), int(eveh.beamType));
            }
            {
                AdditionalDataHeader addh;
                ifs >> addh;
                EQ(true, bool(ifs));
                EQ(std::size_t(1), addh.values.size());
                EQ(std::string("test"), addh.values.at(0).name);
                EQ(std::string("this is a test"), addh.values.at(0).toString());
            }
        }
    }

    void simple()
    {
        RFMFactory &factory = RFMFactory::instance();

        RunInfo::Shared run = factory.createRun(901234, "LAB", 123,
                                                DaqInfo::CrateIdList({1, 2}));
        EQ(true, bool(run));

        {
            MergedFile::Shared file = run->addFile();
            file = run->addFile(); // to force-increment fileNumber
            EQ(true, bool(file));

            for (std::size_t i = 0; i < m_eventCount; ++i)
                file->addEvent(i, i, 2048);

            file->setStatus(MergedFile::WAITING);
            file->save();
            run->setStopDate(std::time(nullptr));
            // merge data first, it updates events offsets and sizes
            MergerTask task(*file);
            task.run();
        }
        MergedFileIndex::Shared file = run->createIndex();
        EQ(true, bool(file));

        std::string err("did not run");
        IndexerTask task(*file);
        task.taskSignal.connect([&err](const std::string &e) { err = e; });

        task.run();
        EQ(std::string(), err);

        const bfs::path path(m_path / "out" / "1970" / "LAB" / "901234" /
                             "stream0");

        checkFile(path / "run901234.idx.finished");
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestIndexerTask);
