/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-08-06T03:24:19+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerRetry : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerRetry);
    CPPUNIT_TEST(retry);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerRetry");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    RunInfo::Shared createRun(RunInfo::RunNumber runNumber = 901234)
    {
        RFMFactory &fact = RFMFactory::instance();
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);

        // to have stopDate > startDate
        RunInfo::Shared run = fact.createRun(runNumber, "EAR1",
                                             std::time(nullptr) - 1, daqs);
        EQ(true, bool(run));
        return run;
    }

    DIMData checkRestored(const std::string &state, unsigned int count)
    {
        RunInfoWaiter waiter;
        DIMData ret;
        EQ(true,
           waiter.wait(
               [&ret, &state, count](DIMData::List &data) {
                   LOG(INFO) << data;
                   ret = getData("901234", DIMData(0, data));
                   return (getData(state + "Count", ret).getULongValue() ==
                           count);
               },
               3000));
        std::time_t startDate = getData("startDate", ret).getUIntValue();
        std::time_t endDate = getData("stopDate", ret).getUIntValue();
        ASSERT(startDate < endDate);
        return ret;
    }

    void retry()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval

        {
            RunInfo::Shared run = createRun();
            run->setApproved(true);

            MergedFile::Shared file;

            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(0, 0, 1024);
            file->setStatus(MergedFile::WAITING);
            file->setRetryDate(std::time(nullptr) + 1);
            file->save();

            run->setStopDate(std::time(nullptr));
            run->save();
        }

        /* manager should find this run and retry it */
        mgr.start();

        checkRestored("migrated", 2); // index
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerRetry);
