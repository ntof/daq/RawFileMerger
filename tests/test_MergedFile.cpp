/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-09T14:20:55+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <ctime>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "RFMFactory.hpp"
#include "data/DaqInfo.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestMergedFile : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestMergedFile);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(events);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path() / "TestMergedFile";
        bfs::create_directories(m_tmp);

        Config::Paths &paths = const_cast<Config::Paths &>(
            Config::instance().getPaths());
        paths.database = (m_tmp / "db.sql").string();
        RFMFactory::destroy(); /* force delete to re-create with new db path */
    }

    void tearDown()
    {
        RFMFactory::destroy(); /* force delete, db will be erased */
        bfs::remove_all(m_tmp);
    }

    void simple()
    {
        RFMFactory &factory = RFMFactory::instance();

        RunInfo::Shared run = factory.createRun(1234, "EAR1", 123,
                                                DaqInfo::CrateIdList());
        EQ(true, bool(run));

        MergedFile::Shared file = run->addFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(0), file->fileNumber());

        file = run->addFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(1), file->fileNumber());
        file->setStatus(MergedFile::WAITING);
        file->setStatus(MergedFile::TRANSFERRING);
        file->setStatus(MergedFile::COPIED);
        file->save();

        run.reset();
        run = factory.getRun(1234);
        EQ(true, bool(run));
        EQ(std::size_t(1), run->fileStats()[MergedFile::COPIED]);

        file = run->currentFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(1), file->fileNumber());
    }

    void events()
    {
        RFMFactory &factory = RFMFactory::instance();

        {
            RunInfo::Shared run = factory.createRun(1234, "EAR1", 123,
                                                    DaqInfo::CrateIdList());
            EQ(true, bool(run));

            MergedFile::Shared file = run->addFile();
            EQ(true, bool(file));

            FileEvent::Shared event = file->addEvent(12, 10, 1024);
            EQ(true, bool(event));
            EQ(false, bool(file->addEvent(12, 11, 1024)));
            EQ(true, bool(file->addEvent(13, 11, 1024)));
        }
        RunInfo::Shared run = factory.getRun(1234);
        EQ(true, bool(run));
        EQ(std::size_t(2), run->currentFile()->eventsCount());
        MergedFile::EventsList list = run->currentFile()->getEvents();
        EQ(std::size_t(2), list.size());
        EQ(FileEvent::SequenceNumber(12), list[0]->sequenceNumber());
        EQ(std::size_t(2048), run->currentFile()->eventsSize());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestMergedFile);
