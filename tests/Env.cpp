/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-18T08:32:15+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "Env.hpp"

#include <fstream>

#include "Config.h"
#include "DaqTypes.h"
#include "RFMFactory.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

using namespace ntof::rfm;
namespace bfs = boost::filesystem;

bfs::path envInit(const std::string &name)
{
    bfs::path tmp = bfs::temp_directory_path() / name;
    bfs::remove_all(tmp);
    bfs::create_directories(tmp);

    const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
    Config::load((dataDir / "conf.xml").string(),
                 (dataDir / "configMisc.xml").string());

    Config::Paths &paths = const_cast<Config::Paths &>(
        Config::instance().getPaths());
    paths.database = (tmp / "db.sql").string();
    paths.local = (tmp / "local").string();
    paths.mount = (tmp / "mnt").string();
    paths.remote = "/castor/cern.ch/user/a/apcdev/tests/RawFileMerger";

    RFMFactory::destroy(); /* force delete to re-create with new db path */
    return tmp;
}

void envDestroy(const bfs::path &env)
{
    bfs::remove_all(env);
}

static void genAcqc(const bfs::path &file,
                    uint32_t type,
                    uint32_t id,
                    uint32_t modCrate)
{
    std::ofstream out;
    out.open(file.string());
    SkipHeader skip(SkipHeader::SIZE,
                    SkipHeader::SIZE + AcquisitionHeader::SIZE + 2048);
    out << skip;

    AcquisitionHeader acqc;
    acqc.dataSize = 2048;
    acqc.detectorType = type;
    acqc.detectorId = id;
    acqc.str_mod_crate = modCrate;
    out << acqc;
    EQ(true, out.good());
    out.close();
    bfs::resize_file(file, skip.end);
}

void genFiles(const bfs::path &path, const size_t count)
{
    std::ofstream out;

    bfs::create_directories(path / "local" / "run901234");
    std::string filename =
        (path / "local" / "run901234" / "run901234.run").string();
    out.open(filename.c_str());

    RunControlHeader rctr;
    rctr.runNumber = 901234;
    rctr.updateExperiment();
    rctr.updateDateTime();
    rctr.totalNumberOfStreams = 2;
    rctr.totalNumberOfModules = 2;
    rctr.totalNumberOfChassis = 2;
    rctr.numberOfChannelsPerModule.resize(2);
    rctr.numberOfChannelsPerModule[0] = 1;
    rctr.numberOfChannelsPerModule[1] = 1;
    out << rctr;

    {
        ModuleHeader modh(2);
        {
            ChannelConfig &cfg = modh.channelsConfig[0];
            cfg.setLocation(1, 1, 0, 0);
            cfg.setDetectorType("TES1");
            cfg.setModuleType("ACQC");
            cfg.detectorId = 0;
            cfg.sampleRate = 1000;
            cfg.sampleSize = 8000;
            cfg.fullScale = 100;
            cfg.setClockState("INTC");
        }
        {
            ChannelConfig &cfg = modh.channelsConfig[1];
            cfg.setLocation(1, 2, 0, 0);
            cfg.setDetectorType("TES2");
            cfg.setModuleType("ACQC");
            cfg.detectorId = 1;
            cfg.sampleRate = 1000;
            cfg.sampleSize = 8000;
            cfg.fullScale = 100;
            cfg.setClockState("INTC");
        }
        out << modh;
    }

    EQ(true, out.good());
    out.close();

    bfs::create_directories(path / "mnt" / "ntofdaq-m111" / "run901234");
    bfs::create_directories(path / "mnt" / "ntofdaq-m222" / "run901234");

    for (std::size_t i = 0; i < count; ++i)
    {
        out.open((path / "local" / "run901234" /
                  ("run901234_" + std::to_string(i) + ".event"))
                     .string());

        {
            EventHeader eveh;
            eveh.eventNumber = i;
            eveh.runNumber = 901234;
            eveh.updateDateTime(std::time(nullptr));
            eveh.setCompTS();
            eveh.beamType = EventHeader::PARASITIC;
            out << eveh;
        }

        { /* this header will have size: 16 + 64 + 8 + 15 + 1 (padding) : 104 */
            AdditionalDataHeader addh;
            AdditionalDataValue value(AdditionalDataValue::TypeChar);
            value.name = "test";
            value.fromString("this is a test");
            addh.values.push_back(value);
            out << addh;
        }

        EQ(true, out.good());
        out.close();

        bfs::path rawPath = path / "mnt" / "ntofdaq-m111" / "run901234" /
            ("run901234_" + std::to_string(i) + "_s1.raw");
        genAcqc(rawPath, *reinterpret_cast<const uint32_t *>("TES1"), 0,
                ChannelConfig::makeLocation(1, 1, 0, 0));

        rawPath = path / "mnt" / "ntofdaq-m222" / "run901234" /
            ("run901234_" + std::to_string(i) + "_s1.raw");
        genAcqc(rawPath, *reinterpret_cast<const uint32_t *>("TES2"), 1,
                ChannelConfig::makeLocation(1, 2, 0, 0));
    }
}
