/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-19T16:24:55+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerRecover : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerRecover);
    CPPUNIT_TEST(restoreFailed);
    CPPUNIT_TEST(restoreApproved);
    CPPUNIT_TEST(restoreEmpty);
    CPPUNIT_TEST(restoreNothing);
    CPPUNIT_TEST(checkCurrentAfterRestore);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerRecover");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    RunInfo::Shared createRun(RunInfo::RunNumber runNumber = 901234)
    {
        RFMFactory &fact = RFMFactory::instance();
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);

        // to have stopDate > startDate
        RunInfo::Shared run = fact.createRun(runNumber, "EAR1",
                                             std::time(nullptr) - 1, daqs);
        EQ(true, bool(run));
        return run;
    }

    DIMData checkRestored(const std::string &state, unsigned int count)
    {
        RunInfoWaiter waiter;
        DIMData ret;
        EQ(true,
           waiter.wait(
               [&ret, &state, count](DIMData::List &data) {
                   LOG(INFO) << data;
                   ret = getData("901234", DIMData(0, data));
                   return (getData(state + "Count", ret).getULongValue() ==
                           count);
               },
               3000));
        std::time_t startDate = getData("startDate", ret).getUIntValue();
        std::time_t endDate = getData("stopDate", ret).getUIntValue();
        ASSERT(startDate < endDate);
        return ret;
    }

    void restoreFailed()
    {
        {
            RunInfo::Shared run = createRun();
            MergedFile::Shared file;

            // error on this file
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(0, 0, 1024);
            file->setStatus(MergedFile::FAILED);
            file->save();

            // this file should be completed and merged
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(1, 1, 1024);
            file->save();

            // this file has no events and should be dropped
            file = run->addFile();
            EQ(true, bool(file));
        }

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        checkRestored("waitingApproval", 3);
        checkRestored("incomplete", 1); // index is incomplete
    }

    void restoreApproved()
    {
        {
            RunInfo::Shared run = createRun();
            MergedFile::Shared file;

            run->setApproved(true);
            run->save();

            // power cut during transfer
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(1, 1, 1024);
            file->setStatus(MergedFile::WAITING);
            file->setStatus(MergedFile::TRANSFERRING); // power cut during
            file->save();

            // already copied, but not yet confirmed file->save();
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(2, 2, 1024);
            file->setStatus(MergedFile::WAITING);
            file->setStatus(MergedFile::TRANSFERRING);
            file->setStatus(MergedFile::COPIED);
            file->save();

            // error on this file
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(3, 3, 1024);
            file->setStatus(MergedFile::FAILED);
            file->save();

            // this file should be completed and merged
            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(4, 4, 1024);
            file->setStatus(MergedFile::INCOMPLETE);
            file->save();

            // this file has no events and should be dropped
            file = run->addFile();
            EQ(true, bool(file));
        }

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        // don't forget the index
        DIMData runData = checkRestored("migrated", 4 + 1);
        checkRestored("ignored", 1);
        ASSERT_THROW(getData("expiryDate", runData), std::invalid_argument);
    }

    void restoreEmpty()
    {
        createRun();

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        checkRestored("incomplete", 1); // index is incomplete

        // Check current is publishing the latestRunNumber
        {
            CurrentRunWaiter waiter;
            EQ(true,
               waiter.wait(
                   [](DIMData::List &data) {
                       DIMData runNumber = getData("runNumber",
                                                   DIMData(0, data));
                       return runNumber.getUIntValue() == 901234;
                   },
                   3000));
        }
    }

    void restoreNothing()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        // Check Current is publishing nothing
        {
            CurrentRunWaiter waiter;
            EQ(true,
               waiter.wait(
                   [](DIMData::List &data) {
                       DIMData runNumber = getData("Number of parameters",
                                                   DIMData(0, data));
                       return runNumber.getIntValue() == 1; // itself only
                   },
                   3000));
        }
    }

    void checkCurrentAfterRestore()
    {
        RunInfo::RunNumber latestRunNumber = 901234;
        {
            RunInfo::Shared run = createRun(latestRunNumber - 1);
            MergedFile::Shared file;

            run->setApproved(true);
            run->save();

            file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(1, 1, 1024);
            file->setStatus(MergedFile::WAITING);
            file->setStatus(MergedFile::TRANSFERRING);
            file->setStatus(MergedFile::COPIED);
            file->save();

            RunInfo::Shared latestRun = createRun(latestRunNumber);
            MergedFile::Shared latestFile;

            latestRun->setApproved(true);
            latestRun->save();

            // power cut during transfer
            latestFile = latestRun->addFile();
            EQ(true, bool(latestFile));
            latestFile->addEvent(1, 1, 1024);
            latestFile->setStatus(MergedFile::WAITING);
            latestFile->setStatus(MergedFile::TRANSFERRING); // power cut during
            latestFile->save();
        }

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        // Check current is publishing the latestRunNumber
        {
            CurrentRunWaiter waiter;
            EQ(true,
               waiter.wait(
                   [&latestRunNumber](DIMData::List &data) {
                       DIMData runNumber = getData("runNumber",
                                                   DIMData(0, data));
                       return runNumber.getUIntValue() == latestRunNumber;
                   },
                   3000));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerRecover);
