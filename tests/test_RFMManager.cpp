/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-19T16:24:55+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <DaqTypes.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Config.h"
#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMManager.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManager : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManager);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(waiting);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManager");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    void simple()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        RFMCli cli;
        cli.listen();

        bool approve = true;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", &approve));

        EQ(true,
           cli.cmdEvent(901234, 0, 0, (AcquisitionHeader::SIZE + 2048) * 2));
        EQ(true,
           cli.cmdEvent(901234, 1, 1, (AcquisitionHeader::SIZE + 2048) * 2));

        EQ(true, cli.cmdStop(901234));

        { // wait for RunInfo to show latest info
            RunInfoWaiter waiter;
            DIMData info;
            EQ(true,
               waiter.wait(
                   [&info](DIMData::List &data) {
                       info = DIMData(0, data);
                       DIMData migrated = getData("901234.migratedCount", info);
                       return (migrated.getULongValue() ==
                               2); // don't forget the index
                   },
                   3000));
            LOG(INFO) << info;
            // /* (RCTR,MODH + (EVEH+ADDH)*2 + ACQC*4) + (RCTR,MODH + INDX +
            // EVEH*2)*/
            EQ(uint64_t((264 + (168 * 2) + (2076 * 4)) + (264 + 64 + (168 * 2))),
               getData("901234.transferred", info).getULongValue());
        }

        { // check files
            DIMData info = DIMData(0, cli.getCurrentFiles());
            // check that migrated file size is accurate
            EQ(uint64_t((264 + (168 * 2) + (2076 * 4))),
               getData("files.0.size", info).getULongValue());
        }
    }

    /**
     * do not approve this run
     * all files must go to waitingApproval, only index remains in incomplete
     */
    void waiting()
    {
        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(500);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        RFMCli cli;
        cli.listen();

        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", nullptr));

        EQ(true,
           cli.cmdEvent(901234, 0, 0, (AcquisitionHeader::SIZE + 2048) * 2));
        EQ(true,
           cli.cmdEvent(901234, 1, 1, (AcquisitionHeader::SIZE + 2048) * 2));

        EQ(true, cli.cmdStop(901234));

        {
            RunInfoWaiter waiter;
            DIMData info;
            EQ(true,
               waiter.wait(
                   [&info](DIMData::List &data) {
                       info = DIMData(0, data);
                       LOG(ERROR) << info;
                       DIMData count = getData("901234.waitingApprovalCount",
                                               info);
                       return (count.getValue<uint64_t>() ==
                               1); // don't forget the index
                   },
                   3000));
            EQ(uint64_t(1),
               getData("901234.incompleteCount", info).getValue<uint64_t>());
        }

        { // check files
            DIMData info = DIMData(0, cli.getCurrentFiles());
            // check that estimated size is reported (sum of reported file sizes)
            EQ(uint64_t((AcquisitionHeader::SIZE + 2048) * 4),
               getData("files.0.size", info).getULongValue());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManager);
