/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-19T10:17:35+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <algorithm>
#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <DaqTypes.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <fcntl.h>

#include "Archiver.hpp"
#include "Config.h"
#include "Env.hpp"
#include "Paths.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "archiver/PreAnalysisArchiver.hpp"
#include "local-config.h"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using namespace ntof::dim;
using ntof::dim::DIMData;

class TestRFMManagerPreAnalysisArchiver : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerPreAnalysisArchiver);
    CPPUNIT_TEST(config);
    CPPUNIT_TEST(liveAnalysis);
    CPPUNIT_TEST(fileRetry);
    CPPUNIT_TEST(fileNoRetry);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerPreAnalysisArchiver");
        {
            bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";

            Config::load((dataDir / "conf_preanalysis.xml").string(),
                         (dataDir / "configMisc.xml").string());

            Config::Paths &paths = const_cast<Config::Paths &>(
                Config::instance().getPaths());
            paths.database = (m_path / "db.sql").string();
            paths.local = (m_path / "local").string();
            paths.mount = (m_path / "mnt").string();
            paths.remote = (m_path / "out").string();

            Archiver::destroy();
        }
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
        {
            /* restore configuration and archiver */
            Config &c(Config::instance());
            const_cast<std::string &>(c.getArchiverName()) = "FakeArchiver";
            Archiver::destroy();
        }
    }

    void sendEvents(DIMDataSet &srv,
                    FileEvent::EventNumber evt,
                    RunNumber runNumber = 901234)
    {
        DIMData::Index idx = 0;
        srv.addData<uint32_t>(idx++, "runNumber", "", runNumber,
                              AddMode::OVERRIDE, false);
        srv.addData<uint32_t>(idx++, "eventNumber", "", evt, AddMode::OVERRIDE,
                              false);
        srv.addData<uint32_t>(idx++, "validatedNumber", "", evt,
                              AddMode::OVERRIDE, false);
        srv.addData<uint64_t>(idx++, "fileSize", "", 1024, AddMode::OVERRIDE,
                              true);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    void config()
    {
        /* ensure archiver can load */
        EQ(PreAnalysisArchiver::NAME, Archiver::instance().name());
    }

    void waitForFiles(
        const std::string &outfile,
        const std::vector<std::string> &names,
        std::vector<std::string> &files,
        const std::chrono::milliseconds &timeout = std::chrono::seconds(3))
    {
        waitFor(
            [&outfile, &files, &names]() {
                std::ifstream ifs(outfile);
                files.clear();
                while (ifs.good())
                {
                    std::string event;
                    ifs >> event;
                    if (!event.empty())
                        files.push_back(event);
                }
                std::vector<std::string> current;
                std::transform(files.begin(), files.end(),
                               std::back_inserter(current),
                               [](const std::string &file) {
                                   return bfs::path(file).filename().string();
                               });
                std::sort(current.begin(), current.end());
                EQ(names, current);
                return true;
            },
            "files not analysed", timeout);
    }

    void liveAnalysis()
    {
        const std::string script = (m_path / "analysis.sh").string();
        const std::string outfile = (m_path / "analysis.txt").string();
        {
            std::ofstream ofs(script.c_str());
            ofs << "#!/bin/sh\n"
                << "echo $1 >> \"" << outfile << "\"";
            ofs.close();
            ::chmod(script.c_str(), 0755);
        }
        Config::instance().setRemoteSubdirs(Config::RemoteSubdirs::NONE);
        Config::PreAnalysis *pre = const_cast<Config::PreAnalysis *>(
            Config::instance().getPreAnalysisConfig());
        if (pre == nullptr)
            throw "pre is null";

        pre->cmd = script;
        pre->skipCount = 2;
        pre->checkErrorCode = true;
        pre->onlyNew = true;
        const std::size_t eventCount = (pre->skipCount + 2) *
            Config::instance().getMaxEvents();
        genFiles(m_path, eventCount);

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(250);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        DIMDataSet srv("EACS/Validation");

        { /* wait for manager to be ready before sending events */
            RFMCli cli;
            cli.listen();
            waitFor([&cli]() { return !cli.getCurrentRun().empty(); },
                    "RFMManager did not mount");
        }

        for (std::size_t i = 0; i < eventCount; ++i)
        {
            sendEvents(srv, i);
        }

        std::vector<std::string> files;
        waitForFiles(outfile,
                     std::vector<std::string>{"run901234_0_s1.raw.finished",
                                              "run901234_3_s1.raw.finished"},
                     files);
        // ensure subdirs=0 has been taken in account
        EQ((m_path / "out" / "run901234_0_s1.raw.finished").string(),
           files.at(0));

        waitFor(
            [&files]() {
                return std::all_of(
                    files.begin(), files.end(),
                    [](const std::string &file) { return !bfs::exists(file); });
            },
            "files not deleted",
            std::chrono::seconds(3)); // leave some time for the deleter
    }

    void fileRetry() { fileRetry(false); }
    void fileNoRetry() { fileRetry(true); }

    void fileRetry(bool onlyNew)
    {
        const std::string script = (m_path / "analysis.sh").string();
        const std::string outfile = (m_path / "analysis.txt").string();
        {
            const std::string testfile = (m_path / "test").string();
            std::ofstream ofs(script.c_str());
            ofs << "#!/bin/sh\n"
                << "if [ ! -e '" << testfile << "' ]; then\n"
                << "  touch '" << testfile << "'\n"
                << "  exit 1\n"
                << "fi\n"
                << "echo $1 >> \"" << outfile << "\"\n";
            ofs.close();
            ::chmod(script.c_str(), 0755);
        }
        Config::instance().setRemoteSubdirs(Config::RemoteSubdirs::RUN);
        Config::PreAnalysis *pre = const_cast<Config::PreAnalysis *>(
            Config::instance().getPreAnalysisConfig());
        if (pre == nullptr)
            throw "pre is null";

        pre->cmd = script;
        pre->skipCount = 1;
        pre->checkErrorCode = true;
        pre->onlyNew = onlyNew;
        const std::size_t eventCount = (pre->skipCount + 2) *
            Config::instance().getMaxEvents();
        genFiles(m_path, eventCount);

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(250);
        mgr.fileDeleter()->wake(); // Take the new interval
        mgr.monitor()->setInterval(500);
        mgr.monitor()->wake(); // Take the new interval
        mgr.start();

        DIMDataSet srv("EACS/Validation");

        { /* wait for manager to be ready before sending events */
            RFMCli cli;
            cli.listen();
            waitFor([&cli]() { return !cli.getCurrentRun().empty(); },
                    "RFMManager did not mount");
        }

        for (std::size_t i = 0; i < eventCount; ++i)
        {
            sendEvents(srv, i);
        }

        std::vector<std::string> expected{"run901234_2_s1.raw.finished"};
        if (!onlyNew)
            expected.insert(expected.begin(), "run901234_0_s1.raw.finished");

        std::vector<std::string> files;
        waitForFiles(outfile, expected, files, std::chrono::seconds(10));

        RunInfo::Shared run = RFMFactory::instance().getRun(901234);
        EQ(true, bool(run));
        RunInfo::FilesList list = run->getFiles();
        EQ(pre->skipCount + 2, list.size());
        if (onlyNew)
        {
            waitFor(
                [&list]() {
                    EQ(MergedFile::IGNORED, list.at(0)->status());
                    return true;
                },
                "file should be ignored on retry", std::chrono::seconds(3));
        }
        waitFor([&files]() { return !bfs::exists(files.at(0)); },
                "migrated file not cleaned-up");

        /* start new run, stop current, flag as old (history=1) to trigger a
         * cleanup */
        sendEvents(srv, 0, 901235);

        bfs::path remotedir = Paths::getRemotePath(*run);
        // remote subdirs enabled, let's wait for deleter to do its job
        waitFor([&remotedir]() { return !bfs::exists(remotedir); },
                "remote directory not cleaned-up", std::chrono::seconds(5));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerPreAnalysisArchiver);
