/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-16T21:22:39+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMXMLService.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Config.h"
#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::dim;

class TestDaqListener : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDaqListener);
    CPPUNIT_TEST(standaloneRun);
    CPPUNIT_TEST(managedRun);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestDaqListener");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    void sendEvents(DIMXMLService &srv)
    {
        for (int i = 0; i < 5; ++i)
        {
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("RunExtensionNumberWrote");
            node.append_attribute("run").set_value(901234);
            node.append_attribute("event").set_value(i);
            node.append_attribute("size").set_value(1024);
            srv.setData(doc);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    void standaloneRun()
    {
        DIMXMLService srv("ntofdaq-m111.cern.ch/WRITER/RunExtensionNumber");
        Config::instance().setListeningMode(Config::ListeningMode::DAQ);
        RFMManager mgr;
        mgr.initialize();
        mgr.start();

        RFMCli cli;

        bool standalone = true;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", nullptr, &standalone));

        sendEvents(srv);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        EQ(true, cli.cmdStop(901234));

        RunInfo::Shared run = RFMFactory::instance().getRun(901234);
        EQ(true, bool(run));

        MergedFile::Shared file = run->currentFile();
        EQ(true, bool(file));
        EQ(size_t(5 * 1024), file->eventsSize());
        EQ(size_t(5), file->eventsCount());
    }

    void managedRun()
    {
        DIMXMLService srv("ntofdaq-m111.cern.ch/WRITER/RunExtensionNumber");
        Config::instance().setListeningMode(Config::ListeningMode::DAQ);
        RFMManager mgr;
        mgr.initialize();
        mgr.start();

        RFMCli cli;

        bool standalone = false;
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);
        EQ(true, cli.cmdStart(901234, daqs, "testEx", nullptr, &standalone));

        sendEvents(srv);
        {
            /* send an event to an unknown run */
            pugi::xml_document doc;
            pugi::xml_node node = doc.append_child("RunExtensionNumberWrote");
            node.append_attribute("run").set_value(901235);
            node.append_attribute("event").set_value(0);
            node.append_attribute("size").set_value(1024);
            srv.setData(doc);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        EQ(true, cli.cmdStop(901234));

        RunInfo::Shared run = RFMFactory::instance().getRun(901234);
        EQ(true, bool(run));

        MergedFile::Shared file = run->currentFile();
        EQ(false, bool(file)); // ensure no events recorded
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDaqListener);
