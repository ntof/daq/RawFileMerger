/*
** Copyright (C) 2021 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-18T18:44:27+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Regex.hpp"
#include "test_helpers.hpp"

class TestRegex : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRegex);
    CPPUNIT_TEST(match);
    CPPUNIT_TEST(search);
    CPPUNIT_TEST(flags);
    CPPUNIT_TEST(error);
    CPPUNIT_TEST_SUITE_END();

public:
    void match()
    {
        Regex r("([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)");
        std::vector<std::string> out;
        EQ(true, r.match("1.2.3", out));
        EQ((std::vector<std::string>{"1.2.3", "1", "2", "3"}), out);

        /* capacity is taken in account */
        out = std::vector<std::string>();
        out.reserve(2);
        EQ(true, r.match("1.2.3", out));
        EQ((std::vector<std::string>{"1.2.3", "1"}), out);

        /* not matching */
        out = std::vector<std::string>(1, "test");
        EQ(false, r.match("test", out));
        EQ((std::vector<std::string>{}), out);
    }

    void search()
    {
        EQ(true, Regex("([[:digit:]]+)").search("1234"));
        EQ(false, Regex("([[:digit:]]+)").search("test"));
    }

    void flags()
    {
        EQ(false, Regex("test.test", Regex::NewLine).search("test\ntest"));
        EQ(true, Regex("test.test", 0).search("test\ntest"));

        EQ(false, Regex("[0-9]a", Regex::Extended).search("1A"));
        EQ(true, Regex("[0-9]a", Regex::Extended | Regex::ICase).search("1A"));
    }

    void error()
    {
        ASSERT_THROW(Regex("(test"), std::invalid_argument);
        ASSERT_THROW(Regex("([[:digit]]"), std::invalid_argument);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRegex);
