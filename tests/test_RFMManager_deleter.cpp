/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-15T16:37:34+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Env.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerDeleter : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerDeleter);
    CPPUNIT_TEST(removeMigrated);
    CPPUNIT_TEST(preserveMigrated);
    CPPUNIT_TEST(keepIgnored);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;
    std::unique_ptr<RFMManager> m_mgr;
    std::size_t m_eventCount;

public:
    void setUp() override
    {
        m_eventCount = 5;
        m_path = envInit("TestRFMManagerDeleter");
        genFiles(m_path, m_eventCount);
        m_dim.reset(new DimTestHelper());
        m_mgr.reset(new RFMManager());
        m_mgr->initialize();
        m_mgr->fileDeleter()->setInterval(100);
        m_mgr->fileDeleter()->wake(); // Take the new interval
        m_mgr->monitor()->setInterval(100);
        m_mgr->monitor()->wake(); // Take the new interval
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
        m_mgr.reset();
    }

    RunInfo::Shared createRun(RunInfo::RunNumber runNumber = 901234)
    {
        RFMFactory &fact = RFMFactory::instance();
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);

        // to have stopDate > startDate
        RunInfo::Shared run = fact.createRun(runNumber, "EAR1",
                                             std::time(nullptr) - 1, daqs);
        EQ(true, bool(run));
        run->setApproved(true);

        MergedFile::Shared file;
        file = run->addFile();
        EQ(true, bool(file));
        for (std::size_t i = 0; i < m_eventCount; ++i)
            file->addEvent(i, i, 1024);
        file->save();
        return run;
    }

    bool checkRawFile()
    {
        return bfs::exists(m_path / "mnt" / "ntofdaq-m111" / "run901234" /
                           "run901234_0_s1.raw");
    }

    bool checkRunDir() { return bfs::exists(m_path / "local" / "run901234"); }

    void removeMigrated()
    {
        EQ(true, checkRawFile());
        EQ(true, checkRunDir());

        {
            RunInfo::Shared run = createRun();
            run->setStopDate(std::time(nullptr));
            run->save();
        }

        /* manager should find this run and retry it */
        m_mgr->start();

        waitFor([this]() { return !checkRawFile(); });
        waitFor([this]() { return !checkRunDir(); });
    }

    void preserveMigrated()
    {
        Config::instance().setDeleterEnabled(false);
        EQ(true, checkRawFile());
        EQ(true, checkRunDir());

        {
            RunInfo::Shared run = createRun();
            run->setStopDate(std::time(nullptr));
            run->save();
        }

        /* manager should find this run and retry it */
        m_mgr->start();

        CPPUNIT_ASSERT_THROW(waitFor([this]() { return !checkRawFile(); }),
                             CPPUNIT_NS::Exception);
        CPPUNIT_ASSERT_THROW(waitFor([this]() { return !checkRunDir(); }),
                             CPPUNIT_NS::Exception);
    }

    void keepIgnored()
    {
        EQ(true, checkRawFile());
        EQ(true, checkRunDir());

        {
            RunInfo::Shared run = createRun();

            /* add an ignored file to the run */
            MergedFile::Shared file = run->addFile();
            EQ(true, bool(file));
            file->addEvent(0, 0, 1024);
            file->save();

            run->setStopDate(std::time(nullptr));
            run->save();
        }

        /* manager should find this run and retry it */
        m_mgr->start();

        waitFor([this]() { return !checkRawFile(); });
        CPPUNIT_ASSERT_THROW(waitFor([this]() { return !checkRunDir(); }),
                             CPPUNIT_NS::Exception);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerDeleter);
