
# Do not use GNUInstallDirs there (would use lib64 instead of lib)
install(FILES rawfilemerger.service
  DESTINATION "lib/systemd/system"
  COMPONENT runtime)
